using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PaymentBackend.Models.Validations;
using Sieve.Attributes;

namespace PaymentBackend.Entities
{
    [Table("Roles")]
    public class Role
    {
        [Key]
        [IsValidRoleId]
        [Sieve(CanFilter = true, CanSort = true)]
        public Guid Id { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public string Name { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public int DiscountPercent { get; set; }
    }
}