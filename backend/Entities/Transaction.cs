using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace PaymentBackend.Entities
{
    [Table("Transactions")]
    public abstract class Transaction
    {
        [Key]
        [Sieve(CanFilter = true, CanSort = true)]
        public Guid Id { get; set; }

        public Account Account { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public Guid AccountId { get; set; }

        public PointOfSale PointOfSale { get; set; }

        [Required]
        public Guid PointOfSaleId { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public decimal Sum { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public DateTime Timestamp { get; set; }

        public string Comment { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public DateTime? Cancelled { get; set; }
    }
}