using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace PaymentBackend.Entities
{
    [Table("PurchaseTransactionEntries")]
    public class PurchaseTransactionEntry
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid ProductId { get; set; }

        public Product Product { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public Guid PurchaseTransactionId { get; set; }

        public PurchaseTransaction PurchaseTransaction { get; set; }
    }
}