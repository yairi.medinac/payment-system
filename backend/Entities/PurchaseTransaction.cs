using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentBackend.Entities
{
    [Table("PurchaseTransactions")]
    public class PurchaseTransaction : Transaction
    {
        public List<PurchaseTransactionEntry> Entries { get; set; }
    }
}