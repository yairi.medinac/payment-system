using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentBackend.Entities
{
    [Table("AccountRoles")]
    public class AccountRole
    {
        public Guid AccountId { get; set; }
        public Account Account { get; set; }

        public Guid RoleId { get; set; }
        public Role Role { get; set; }
    }
}