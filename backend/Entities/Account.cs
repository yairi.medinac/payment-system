using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sieve.Attributes;

namespace PaymentBackend.Entities
{
    [Table("Accounts")]
    public class Account
    {
        [Key]
        [Sieve(CanFilter = true, CanSort = true)]
        public Guid Id { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string PasswordSalt { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public string CardId { get; set; }

        [Required]
        [Sieve(CanFilter = true, CanSort = true)]
        public decimal Balance { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public bool Locked { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        [Required]
        public int Overdraft { get; set; }

        public IList<AccountRole> Roles { get; set; }
    }
}