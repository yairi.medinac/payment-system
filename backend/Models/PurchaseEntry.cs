using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Models.Validations;

namespace PaymentBackend.Models
{
    public class PurchaseEntry
    {
        [Required]
        [IsValidProductId]
        public Guid ProductId { get; set; }

        [Required]
        [Range(0, 100)]
        public decimal Price { get; set; }

        public override string ToString()
        {
            return $"{nameof(ProductId)}: {ProductId}, {nameof(Price)}: {Price}";
        }
    }
}