using System;
using System.Collections.Generic;
using PaymentBackend.Entities;

namespace PaymentBackend.Models
{
    public class TransactionResponse
    {
        public Guid Id { get; set; }

        public AccountInfo Account { get; set; }

        public PointOfSaleResponse PointOfSale { get; set; }

        public decimal Sum { get; set; }

        public DateTime Timestamp { get; set; }

        public string Comment { get; set; }
        
        public DateTime? Cancelled { get; set; }

        public List<PurchaseTransactionEntry> Entries { get; set; }

        public TransactionType TransactionType => Entries == null
            ? TransactionType.ChargeTransaction
            : TransactionType.PurchaseTransaction;
    }
}