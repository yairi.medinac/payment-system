using System.Runtime.Serialization;

namespace PaymentBackend.Models
{
    public class LoginResponse
    {
        [DataMember(Name = "token")]
        public string Token { get; set; }

        public override string ToString()
        {
            return $"{nameof(Token)}: {Token}";
        }
    }
}