using System;
using System.ComponentModel.DataAnnotations;

namespace PaymentBackend.Models
{
    public class PointOfSaleDevice
    {
        [Required]
        public Guid PosId { get; set; }

        [Required]
        public PosClientType Type { get; set; }

        public override string ToString()
        {
            return $"PosDevice({nameof(Type)}: {Type.ToString()}) {nameof(PosId)}: {PosId}";
        }
    }
}