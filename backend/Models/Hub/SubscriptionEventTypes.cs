namespace PaymentBackend.Models.Hub
{
    public enum SubscriptionEventTypes
    {
        PosAdded,
        PosConfigured,
        PosDeviceLeft,
        PosDeviceJoined
    }
}