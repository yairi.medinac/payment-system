using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PaymentBackend.Models.Hub
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SubscriptionEventClasses
    {
        PointOfSaleInfo
    }
}