namespace PaymentBackend.Models.Hub
{
    public class SubscriptionNotificationPointOfSaleInfo : SubscriptionNotification
    {
        public PointOfSaleInfo Content { get; set; }
    }
}