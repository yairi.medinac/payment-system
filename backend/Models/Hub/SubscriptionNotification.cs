namespace PaymentBackend.Models.Hub
{
    public abstract class SubscriptionNotification
    {
        public SubscriptionEventClasses EventClass { get; set; }
        public SubscriptionEventTypes EventType { get; set; }
    }
}