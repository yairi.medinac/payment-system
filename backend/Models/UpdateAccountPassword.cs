using System.ComponentModel.DataAnnotations;
using PaymentBackend.Models.Validations;

namespace PaymentBackend.Models
{
    public class UpdateAccountPassword: BaseUpdateMessage
    {
        [Required]
        [MinLength(8)]
        public string Password { get; set; }
    }
}