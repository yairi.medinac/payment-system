using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Entities;

namespace PaymentBackend.Models
{
    public class ProductResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public ProductTypeResponse ProductType { get; set; }
        
        public int Order { get; set; }
        
        public bool Locked { get; set; }
    }
}