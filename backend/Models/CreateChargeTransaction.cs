using System.ComponentModel.DataAnnotations;

namespace PaymentBackend.Models
{
    public class CreateChargeTransaction : CreateTransactionBase
    {
        [Required]
        [Range(-100, 100)]
        public decimal Amount { get; set; }

        public override string ToString()
        {
            return $"{nameof(Amount)}: {Amount}, {nameof(AccountId)}: {AccountId}";
        }
    }
}