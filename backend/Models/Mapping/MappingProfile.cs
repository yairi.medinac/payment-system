using System.Linq;
using AutoMapper;
using PaymentBackend.Entities;

namespace PaymentBackend.Models.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<PurchaseEntry, PurchaseTransactionEntry>()
                .ForMember(x => x.Id, opts => opts.Ignore())
                .ForMember(x => x.PurchaseTransaction, opts => opts.Ignore())
                .ForMember(x => x.Product, opts => opts.Ignore());

            CreateMap<ChargeTransaction, TransactionResponse>()
                .ForMember(x => x.Entries, opts => opts.Ignore());
            CreateMap<PurchaseTransaction, TransactionResponse>()
                .AfterMap((transaction, response) =>
                    response.Entries.ForEach(entry =>
                    {
                        entry.Product.Image = null;
                        entry.PurchaseTransaction = null;
                    }));

            CreateMap<Account, CardInfo>()
                .ForMember(dest => dest.Id,
                    opts => opts.MapFrom(src => src.CardId))
                .ForMember(dest => dest.Account,
                    opts => opts.MapFrom(src => src));

            CreateMap<AccountRole, Role>()
                .ForMember(dest => dest.Id,
                    opts => opts.MapFrom(src => src.Role.Id))
                .ForMember(dest => dest.Name,
                    opts => opts.MapFrom(src => src.Role.Name))
                .ForMember(dest => dest.DiscountPercent,
                    opts => opts.MapFrom(src => src.Role.DiscountPercent));

            CreateMap<CreateAccount, Account>()
                .ForMember(dest => dest.Id, opts => opts.Ignore())
                .ForMember(dest => dest.Balance, opts => opts.Ignore())
                .ForMember(dest => dest.PasswordSalt, opts => opts.Ignore());

            CreateMap<UpdateAccount, Account>()
                .ForMember(dest => dest.Id, opts => opts.Ignore())
                .ForMember(dest => dest.Balance, opts => opts.Ignore())
                .ForMember(dest => dest.Password, opts => opts.Ignore())
                .ForMember(dest => dest.PasswordSalt, opts => opts.Ignore())
                .AfterMap((ua, a) =>
                {
                    a.Roles = a.Roles.Select(ar =>
                    {
                        ar.AccountId = ua.Id;
                        return ar;
                    }).ToList();
                });

            CreateMap<Role, AccountRole>()
                .ForMember(dest => dest.Role, opts => opts.Ignore())
                .ForMember(dest => dest.RoleId, opts => opts.MapFrom(s => s.Id))
                .ForMember(dest => dest.AccountId, opts => opts.Ignore())
                .ForMember(dest => dest.Account, opts => opts.Ignore());

            CreateMap<UpdateAccountPassword, Account>()
                .ForMember(dest => dest.Id, opts => opts.Ignore())
                .ForMember(dest => dest.Balance, opts => opts.Ignore())
                .ForMember(dest => dest.Roles, opts => opts.Ignore())
                .ForMember(dest => dest.Locked, opts => opts.Ignore())
                .ForMember(dest => dest.Username, opts => opts.Ignore())
                .ForMember(dest => dest.CardId, opts => opts.Ignore())
                .ForMember(dest => dest.PasswordSalt, opts => opts.Ignore());

            CreateMap<CreateProduct, Product>()
                .ForMember(dest => dest.Id, opts => opts.Ignore())
                .ForMember(dest => dest.ProductType, opts => opts.Ignore());

            CreateMap<CreatePointOfSale, PointOfSale>()
                .ForMember(dest => dest.Id, opts => opts.Ignore());

            CreateMap<UpdatePointOfSale, PointOfSale>()
                .ForMember(dest => dest.Id, opts => opts.Ignore());

            CreateMap<Product, ProductResponse>()
                .ForMember(dest => dest.Image, opts =>
                {
                    opts.Condition(p => !string.IsNullOrWhiteSpace(p.Image));
                    opts.MapFrom(p => $"/api/v1/Products/{p.Id}/image");
                });
            
            CreateMap<ProductType, ProductTypeResponse>()
                .ForMember(dest => dest.Image, opts =>
                {
                    opts.Condition(p => !string.IsNullOrWhiteSpace(p.Image));
                    opts.MapFrom(p => $"/api/v1/ProductTypes/{p.Id}/image");
                });

            CreateMap<UpdateProduct, Product>()
                .ForAllMembers(o => o.Condition((src, dst, value) => { return value != null; }));

            CreateMap<CreateProductType, ProductType>()
                .ForMember(dest => dest.Id, opts => opts.Ignore());

            CreateMap<UpdateProductType, ProductType>()
                .ForMember(dest => dest.Id, opts => opts.Ignore());
        }
    }
}