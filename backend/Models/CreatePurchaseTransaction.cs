using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PaymentBackend.Models
{
    public class CreatePurchaseTransaction : CreateTransactionBase
    {
        [Required]
        [MinLength(1)]
        public List<PurchaseEntry> Entries { get; set; }

        public override string ToString()
        {
            return $"{nameof(Entries)}: {Entries}, {nameof(AccountId)}: {AccountId}";
        }
    }
}