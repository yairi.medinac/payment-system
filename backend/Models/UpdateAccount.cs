using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Entities;
using PaymentBackend.Models.Validations;

namespace PaymentBackend.Models
{
    public class UpdateAccount : BaseUpdateMessage
    {
        [Required]
        [StringLength(128, MinimumLength = 1)]
        [IsUniqueUsername]
        public string Username { get; set; }

        [IsUniqueCardId]
        [StringLength(255, MinimumLength = 1)]
        public string CardId { get; set; }

        [Required]
        public IList<Role> Roles { get; set; }

        [Required]
        public bool Locked { get; set; }
        
        [Range(0, int.MaxValue)]
        public int Overdraft { get; set; }
    }
}