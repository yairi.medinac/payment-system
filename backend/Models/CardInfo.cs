namespace PaymentBackend.Models
{
    public class CardInfo
    {
        public string Id { get; set; }
        public AccountInfo Account { get; set; }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(Account)}: {Account}";
        }
    }
}