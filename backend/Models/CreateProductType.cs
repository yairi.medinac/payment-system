using System.ComponentModel.DataAnnotations;
using PaymentBackend.Models.Validations;

namespace PaymentBackend.Models
{
    public class CreateProductType
    {
        [Required]
        [IsUniqueProductType]
        public string Name { get; set; }

        public int Order { get; set; }

        public string Image { get; set; }
        
        public bool Locked { get; set; }
    }
}