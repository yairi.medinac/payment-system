using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Entities;
using PaymentBackend.Models.Validations;

namespace PaymentBackend.Models
{
    public class CreateAccount
    {
        [Required]
        [MinLength(1)]
        [MaxLength(128)]
        [IsUniqueUser]
        public string Username { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }

        public string CardId { get; set; }

        [Range(0, int.MaxValue)]
        public int Overdraft { get; set; }

        [Required]
        public IList<Role> Roles { get; set; }
    }
}