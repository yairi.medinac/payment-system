using System.ComponentModel.DataAnnotations;
using PaymentBackend.Models.Validations;

namespace PaymentBackend.Models
{
    public class CreatePointOfSale
    {
        [Required]
        [MinLength(1)]
        [MaxLength(128)]
        [IsUniquePointOfSale]
        public string Name { get; set; }
    }
}