using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Models.Validations;

namespace PaymentBackend.Models
{
    public class CreateTransactionBase
    {
        [Required]
        [IsValidAccountId]
        public Guid AccountId { get; set; }

        [Required]
        [IsValidPointOfSaleId(onlyActives: true)]
        public Guid PointOfSaleId { get; set; }

        public string Comment { get; set; }
    }
}