using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Services;

namespace PaymentBackend.Models.Validations
{
    public class IsUniqueProductType : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var productTypeName = (string) value;
            var productTypeId = Guid.Empty;
            if (validationContext.ObjectInstance is BaseUpdateMessage instance)
            {
                productTypeId = instance.Id;
            }

            var productTypeRepository =
                (IProductTypeRepository) validationContext.GetService(typeof(IProductTypeRepository));
            var existingProductType = productTypeRepository?.GetProductTypeByNameAsync(productTypeName).Result;
            return existingProductType != null && existingProductType.Id != productTypeId
                ? new ValidationResult(GetErrorMessage(productTypeName))
                : ValidationResult.Success;
        }

        private static string GetErrorMessage(string productTypeName)
        {
            return $"A product type with the name {productTypeName} does already exist.";
        }
    }
}