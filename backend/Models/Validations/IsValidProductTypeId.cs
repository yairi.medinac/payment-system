using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Services;

namespace PaymentBackend.Models.Validations
{
    public class IsValidProductTypeId : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var productTypeId = (Guid) value;
            var productTypeRepository = (IProductTypeRepository) validationContext.GetService(typeof(IProductTypeRepository));
            var existingProductType = productTypeRepository?.GetProductTypeByIdAsync(productTypeId).Result;
            return existingProductType == null ? new ValidationResult(GetErrorMessage(productTypeId)) : ValidationResult.Success;
        }

        private static string GetErrorMessage(Guid productTypeId)
        {
            return $"An product type with the id {productTypeId} does not exist.";
        }
    }
}