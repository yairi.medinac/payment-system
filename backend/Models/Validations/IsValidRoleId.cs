using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Services;

namespace PaymentBackend.Models.Validations
{
    public class IsValidRoleId : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var roleId = (Guid) value;
            var rolesRepository = (IRolesRepository) validationContext.GetService(typeof(IRolesRepository));
            var existingRole = rolesRepository?.GetRoleByIdAsync(roleId).Result;
            return existingRole == null ? new ValidationResult(GetErrorMessage(roleId)) : ValidationResult.Success;
        }

        private static string GetErrorMessage(Guid roleId)
        {
            return $"An role with the id {roleId} does not exist.";
        }
    }
}