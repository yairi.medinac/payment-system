using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Services;

namespace PaymentBackend.Models.Validations
{
    public class IsUniquePointOfSale : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var posName = (string) value;
            var posId = Guid.Empty;
            if (validationContext.ObjectInstance is BaseUpdateMessage instance)
            {
                posId = instance.Id;
            }
            
            var pointOfSaleRepository = (IPointOfSaleRepository) validationContext.GetService(typeof(IPointOfSaleRepository));
            var pointOfSale = pointOfSaleRepository?.GetPointOfSaleByNameAsync(posName).Result;
            return pointOfSale != null && pointOfSale.Id != posId ? new ValidationResult(GetErrorMessage(posName)) : ValidationResult.Success;
        }

        private static string GetErrorMessage(string posName)
        {
            return $"An point of sale with the name {posName} already exists.";
        }
    }
}