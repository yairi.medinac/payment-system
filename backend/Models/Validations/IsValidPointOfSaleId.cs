using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Services;

namespace PaymentBackend.Models.Validations
{
    public class IsValidPointOfSaleId : ValidationAttribute
    {
        public IsValidPointOfSaleId(bool onlyActives = false)
        {
            OnlyActives = onlyActives;
        }

        public bool OnlyActives { get; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var posId = (Guid) value;
            var pointOfSaleRepository =
                (IPointOfSaleRepository) validationContext.GetService(typeof(IPointOfSaleRepository));
            var existingPointOfSale = pointOfSaleRepository?.GetPointOfSaleByIdAsync(posId).Result;
            return existingPointOfSale == null
                   || (OnlyActives && !existingPointOfSale.HasNfcClient)
                ? new ValidationResult(GetErrorMessage(posId))
                : ValidationResult.Success;
        }

        private string GetErrorMessage(Guid posId)
        {
            return "An " + (OnlyActives ? "NFC ready " : "") + $"point of sale with the id {posId} does not exist.";
        }
    }
}