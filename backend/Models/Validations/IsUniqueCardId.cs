using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Services;

namespace PaymentBackend.Models.Validations
{
    public class IsUniqueCardId : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var cardId = (string) value;
            var userId = Guid.Empty;
            if (validationContext.ObjectInstance is BaseUpdateMessage instance)
            {
                userId = instance.Id;
            }

            var accountRepository =
                (IAccountRepository) validationContext.GetService(typeof(IAccountRepository));
            var existingAccount = accountRepository?.GetAccountInformationByCardIdAsync(cardId).Result;
            return existingAccount != null && existingAccount.Id != userId
                ? new ValidationResult(GetErrorMessage(cardId))
                : ValidationResult.Success;
        }

        private static string GetErrorMessage(string cardId)
        {
            return $"The given card id ({cardId}) is already assigned to another account.";
        }
    }
}