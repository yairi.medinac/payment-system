using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Services;

namespace PaymentBackend.Models.Validations
{
    public class IsUniqueProduct : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var product = (string) value;
            var productId = Guid.Empty;
            if (validationContext.ObjectInstance is BaseUpdateMessage instance)
            {
                productId = instance.Id;
            }

            var productRepository = (IProductRepository) validationContext.GetService(typeof(IProductRepository));
            var existingProduct = productRepository?.GetProductByNameAsync(product).Result;
            return existingProduct != null && existingProduct.Id != productId
                ? new ValidationResult(GetErrorMessage(product))
                : ValidationResult.Success;
        }

        private static string GetErrorMessage(string product)
        {
            return $"An product with the name {product} already exists.";
        }
    }
}