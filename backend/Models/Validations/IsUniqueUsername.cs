using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Services;

namespace PaymentBackend.Models.Validations
{
    public class IsUniqueUsername : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var userName = (string) value;
            var userId = Guid.Empty;
            if (validationContext.ObjectInstance is BaseUpdateMessage instance)
            {
                userId = instance.Id;
            }

            var accountRepository = (IAccountRepository) validationContext.GetService(typeof(IAccountRepository));
            var existingAccount = accountRepository?.GetAccountByUsernameAsync(userName).Result;
            return existingAccount != null && existingAccount.Id != userId
                ? new ValidationResult(GetErrorMessage(userName))
                : ValidationResult.Success;
        }

        private static string GetErrorMessage(string userName)
        {
            return $"An user with the name {userName} does already exist.";
        }
    }
}