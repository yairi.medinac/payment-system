using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Services;

namespace PaymentBackend.Models.Validations
{
    public class IsUniqueUser : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var username = (string) value;
            var userId = Guid.Empty;
            if (validationContext.ObjectInstance is BaseUpdateMessage instance)
            {
                userId = instance.Id;
            }

            var accountRepository = (IAccountRepository) validationContext.GetService(typeof(IAccountRepository));
            var existingAccount = accountRepository?.GetAccountByUsernameAsync(username).Result;
            return existingAccount != null && existingAccount.Id != userId
                ? new ValidationResult(GetErrorMessage(username))
                : ValidationResult.Success;
        }

        private static string GetErrorMessage(string username)
        {
            return $"An account with the username {username} already exists.";
        }
    }
}