using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Logging;

namespace PaymentBackend.Models.Validations
{
    public class MapIdToUpdateMessageFilter : IActionFilter, IOrderedFilter
    {
        public const int FilterOrder = -2100;

        public MapIdToUpdateMessageFilter()
        {
        }

        public int Order => FilterOrder;

        public bool IsReusable => true;

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.Request.Method != HttpMethods.Put)
            {
                return;
            }

            if (context.Result != null)
            {
                return;
            }

            if (!context.ActionArguments.TryGetValue("id", out var id))
            {
                return;
            }

            if (!(id is Guid guid))
            {
                return;
            }

            var baseMessage =
                (BaseUpdateMessage) context.ActionArguments.SingleOrDefault(kvp => kvp.Value is BaseUpdateMessage).Value;
            if (baseMessage == null)
            {
                return;
            }

            // all the magic is here
            baseMessage.Id = guid;

            // we need to revalidate afterwards
            var controller = (ControllerBase) context.Controller;
            controller.ControllerContext.ModelState.Clear();
            controller.ObjectValidator.Validate(controller.ControllerContext, null, string.Empty, baseMessage);
        }
    }
}