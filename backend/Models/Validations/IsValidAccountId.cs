using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Services;

namespace PaymentBackend.Models.Validations
{
    public class IsValidAccountId : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var accountId = (Guid) value;
            var accountRepository = (IAccountRepository) validationContext.GetService(typeof(IAccountRepository));
            var account = accountRepository?.GetAccountInformationByIdAsync(accountId).Result;
            return account == null ? new ValidationResult(GetErrorMessage(accountId)) : ValidationResult.Success;
        }

        private static string GetErrorMessage(Guid accountId)
        {
            return $"An account with the id {accountId} does not exist.";
        }
    }
}