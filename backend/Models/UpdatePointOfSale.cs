using System.ComponentModel.DataAnnotations;
using PaymentBackend.Models.Validations;

namespace PaymentBackend.Models
{
    public class UpdatePointOfSale: BaseUpdateMessage
    {
        [Required]
        [MinLength(1)]
        [MaxLength(128)]
        public string Name { get; set; }
    }
}