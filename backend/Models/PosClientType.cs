namespace PaymentBackend.Models
{
    public enum PosClientType
    {
        CashDesk = 0,
        NfcReader = 1,
        EventSubscriber = 2
    }
}