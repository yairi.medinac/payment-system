using System;

namespace PaymentBackend.Models
{
    public class ProductTypeResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }

        public string Image { get; set; }

        public bool Locked { get; set; }
    }
}