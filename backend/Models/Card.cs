namespace PaymentBackend.Models
{
    public class Card
    {
        public string Id { get; set; }

        public override string ToString()
        {
            return $"Card: {nameof(Id)}: {Id}";
        }
    }
}