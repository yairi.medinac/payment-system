using System;

namespace PaymentBackend.Models
{
    public class PointOfSaleResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}