using System;
using Sieve.Attributes;

namespace PaymentBackend.Models
{
    public class PointOfSaleInfo
    {
        [Sieve(CanFilter = true, CanSort = true)]
        public Guid Id { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public string Name { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public bool HasNfcClient { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public bool HasCashDeskClient { get; set; }

        [Sieve(CanFilter = true, CanSort = true)]
        public bool IsReady => HasNfcClient && HasCashDeskClient;
    }
}