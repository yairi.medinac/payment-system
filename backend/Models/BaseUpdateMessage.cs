using System;

namespace PaymentBackend.Models
{
    public abstract class BaseUpdateMessage
    {
        internal Guid Id { get; set; }
    }
}