using System;
using System.ComponentModel.DataAnnotations;
using PaymentBackend.Models.Validations;

namespace PaymentBackend.Models
{
    public class CreateProduct
    {
        [Required]
        [IsUniqueProduct]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public decimal Price { get; set; }

        public string Description { get; set; }
        
        public string Image { get; set; }

        [Required]
        [IsValidProductTypeId]
        public Guid ProductTypeId { get; set; }
        
        public int Order { get; set; }
        
        public bool Locked { get; set; }
    }
}
