using System;
using System.Collections.Generic;
using PaymentBackend.Entities;

namespace PaymentBackend.Models
{
    public class AccountInfo
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string CardId { get; set; }
        public decimal Balance { get; set; }
        public bool Locked { get; set; }
        public List<Role> Roles { get; set; }
        public int Overdraft { get; set; }

        public override string ToString()
        {
            return
                $"{nameof(Id)}: {Id}, {nameof(Username)}: {Username}, {nameof(CardId)}: {CardId}, {nameof(Balance)}: {Balance}, {nameof(Locked)}: {Locked}";
        }
    }
}