namespace PaymentBackend.Models
{
    public enum TransactionType
    {
        ChargeTransaction = 0,
        PurchaseTransaction = 1
    }
}