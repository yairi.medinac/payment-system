using System;
using Microsoft.EntityFrameworkCore;
using PaymentBackend.Entities;

namespace PaymentBackend.Context
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<PointOfSale> PointsOfSale { get; set; }

        public DbSet<PurchaseTransaction> PurchaseTransactions { get; set; }
        public DbSet<ChargeTransaction> ChargeTransactions { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Transaction>()
                .HasOne(t => t.PointOfSale)
                .WithMany(pos => pos.Transactions)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Account>()
                .Property(account => account.Balance)
                .HasDefaultValue(0.0m);

            modelBuilder.Entity<Account>()
                .HasIndex(account => account.CardId)
                .IsUnique();

            modelBuilder.Entity<Account>()
                .HasIndex(account => account.Username)
                .IsUnique();

            modelBuilder.Entity<ProductType>()
                .HasIndex(pt => pt.Name)
                .IsUnique();

            modelBuilder.Entity<ProductType>()
                .Property(pt => pt.Locked)
                .HasDefaultValue(false);

            modelBuilder.Entity<Product>()
                .HasIndex(p => p.Name)
                .IsUnique();

            modelBuilder.Entity<Product>()
                .Property(p => p.Locked)
                .HasDefaultValue(false);

            modelBuilder.Entity<PointOfSale>()
                .HasIndex(pos => pos.Name)
                .IsUnique();

            modelBuilder.Entity<AccountRole>()
                .HasKey(x => new {x.AccountId, x.RoleId});

            modelBuilder.Entity<Role>().HasData(
                new Role
                {
                    Id = Guid.Parse("334b3221-4d17-445e-9b75-317868a161ff"),
                    Name = "Admin",
                    DiscountPercent = 0
                },
                new Role
                {
                    Id = Guid.Parse("525d09dd-ceea-4851-a7cd-8223c4c24689"),
                    Name = "User",
                    DiscountPercent = 0
                },
                new Role
                {
                    Id = Guid.Parse("bfd373c2-c652-4f43-9f3d-d9cdd0b4ceb8"),
                    Name = "NfcClient",
                    DiscountPercent = 0
                },
                new Role
                {
                    Id = Guid.Parse("c5ccdf94-bc36-42f2-9e5f-5a1a22bb6360"),
                    Name = "CashDesk",
                    DiscountPercent = 0
                },
                new Role
                {
                    Id = Guid.Parse("28376b8e-9a34-41a3-ba69-3732902b9fe4"),
                    Name = "Member",
                    DiscountPercent = 10
                }
            );

            // seed the database with dummy data
            modelBuilder.Entity<Account>().HasData(
                new Account
                {
                    Id = Guid.Parse("de89dcc7-f39f-41d2-9900-1df0bd440ed9"),
                    Balance = 0.0m,
                    Password = "bf8cb09310e40a1857562adca4f612502b35bbb5ad46900b825a3fdc0d6c9fbf", // password is adminadmin
                    PasswordSalt = "f2361a6be65a5313b83e9d6e8a165f21",
                    CardId = null,
                    Username = "admin",
                    Locked = false
                }
            );

            modelBuilder.Entity<AccountRole>().HasData(
                new AccountRole
                {
                    AccountId = Guid.Parse("de89dcc7-f39f-41d2-9900-1df0bd440ed9"),
                    RoleId = Guid.Parse("334b3221-4d17-445e-9b75-317868a161ff")
                }
            );
        }
    }
}