﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using PaymentBackend.Context;
using PaymentBackend.Filters;
using PaymentBackend.Helpers;
using PaymentBackend.Helpers.Sieve;
using PaymentBackend.Hubs;
using PaymentBackend.Models.Mapping;
using PaymentBackend.Models.Validations;
using PaymentBackend.Services;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Sieve.Models;
using Sieve.Services;

namespace PaymentBackend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void ConfigureServices(IServiceCollection services)
        {
            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();

            services.AddScoped<ISieveProcessor, ApplicationSieveProcessor>();
            services.Configure<SieveOptions>(Configuration.GetSection("Sieve"));
            services.AddScoped<ISieveCustomSortMethods, SieveCustomSortMethods>();
            services.AddScoped<ISieveCustomFilterMethods, SieveCustomFilterMethods>();

            // Add framework services.
            services.AddDbContext<PaymentContext>(options =>
            {
                options.UseMySql(
                    Configuration.GetConnectionString("DefaultConnection"),
                    mySqlOptions => { mySqlOptions.ServerVersion(new Version(10, 4, 7), ServerType.MariaDb); }
                );
            });

            services.AddMvc(options =>
                {
                    options.Filters.Add(typeof(MapIdToUpdateMessageFilter), MapIdToUpdateMessageFilter.FilterOrder);
                    options.Filters.Add(typeof(PagedResultResponseFilter));
                })
                .AddJsonOptions(
                    options =>
                    {
                        options.JsonSerializerOptions.Converters.Add(new DateTimeToAtomStringConverter());
                        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                        options.JsonSerializerOptions.IgnoreNullValues = true;
                        options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                    })
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressInferBindingSourcesForParameters = false; // not disable binding source inference
                    options.SuppressMapClientErrors = false; // automatically create ProblemDetails
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Payment System API", Version = "v1"});

                // c.DescribeAllEnumsAsStrings();

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
                c.AddSecurityDefinition("JwtToken", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    In = ParameterLocation.Header
                });
                c.OperationFilter<AuthorizeCheckOperationFilter>();
            });

            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddCors();

            var jwtSecretKey = Encoding.ASCII.GetBytes(appSettings.JwtSecret);
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(o =>
                    {
                        o.RequireHttpsMetadata = false;
                        o.SaveToken = true;
                        o.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(jwtSecretKey),
                            ValidateIssuer = false,
                            ValidateAudience = false,
                            ValidateLifetime = true
                        };
                        // We have to hook the OnMessageReceived event in order to
                        // allow the JWT authentication handler to read the access
                        // token from the query string when a WebSocket or 
                        // Server-Sent Events request comes in.
                        o.Events = new JwtBearerEvents
                        {
                            OnMessageReceived = context =>
                            {
                                var accessToken = context.Request.Query["access_token"];

                                // If the request is for our hub...
                                var path = context.HttpContext.Request.Path;
                                if (!string.IsNullOrEmpty(accessToken) &&
                                    (path.StartsWithSegments("/payment-hub")))
                                {
                                    // Read the token out of the query string
                                    context.Token = accessToken;
                                }

                                return Task.CompletedTask;
                            }
                        };
                    }
                );

            services.AddSignalR(options => { options.EnableDetailedErrors = true; })
                .AddJsonProtocol(o => o.PayloadSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IProductTypeRepository, ProductTypeRepository>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IRolesRepository, RolesRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<IPointOfSaleRepository, PointOfSaleRepository>();
            services.AddSingleton<IConnectionService, ConnectionService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, PaymentContext dbContext)
        {
            if (!env.IsDevelopment())
            {
                dbContext.Database.Migrate();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Payment System API v1"); });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors();
            //app.UseHttpsRedirection();
            app.UseAuthentication();

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<PaymentHub>("/payment-hub");
            });

            // serve static files from wwwroot
            app.UseDefaultFiles();
            var fileExtensionContentTypeProvider = new FileExtensionContentTypeProvider
            {
                Mappings = {[".webmanifest"] = "application/manifest+json"}
            };
            app.UseStaticFiles(new StaticFileOptions
            {
                ContentTypeProvider = fileExtensionContentTypeProvider
            });

            // map /cashdesk and /admin as SPA's from within wwwroot
            if (!env.IsDevelopment())
            {
                app.Map("/cashdesk", cashDeskUi =>
                {
                    cashDeskUi.UseSpa(spa =>
                    {
                        spa.Options.SourcePath = Path.Combine("wwwroot", "cashdesk");
                        spa.Options.DefaultPageStaticFileOptions = new StaticFileOptions
                        {
                            FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "cashdesk")),
                            ContentTypeProvider = fileExtensionContentTypeProvider
                        };
                    });
                });
                app.Map("/admin", adminUi =>
                {
                    adminUi.UseSpa(spa =>
                    {
                        spa.Options.SourcePath = Path.Combine("wwwroot", "admin");
                        spa.Options.DefaultPageStaticFileOptions = new StaticFileOptions
                        {
                            FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "admin")),
                            ContentTypeProvider = fileExtensionContentTypeProvider
                        };
                    });
                });
            }
        }
    }
}