using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaymentBackend.Models;
using PaymentBackend.Models.Hub;

namespace PaymentBackend.Hubs
{
    public interface IPaymentHub
    {
        Task NewCardReader(PointOfSaleInfo pointOfSale);
        Task NewCashDesk(PointOfSaleInfo pointOfSale);
        Task NewEventSubscriber();
        Task JoinRequired(string message);
        Task Welcome(PointOfSaleInfo pointOfSale);
        Task Error(ProblemDetails problemDetails);
        Task DeviceLeftPos(PointOfSaleDevice pointOfSaleDeviceMessage);
        Task NewCard(CardInfo cardInfo);
        Task RemovedCard();
        Task GetCard();
        Task Error(string message);
        Task NewEvent(SubscriptionNotification notificationPointOfSaleInfo);
    }
}