using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using PaymentBackend.Exceptions;
using PaymentBackend.Models;
using PaymentBackend.Models.Hub;
using PaymentBackend.Services;
using Sieve.Models;

namespace PaymentBackend.Hubs
{
    [Authorize]
    public class PaymentHub : Hub<IPaymentHub>
    {
        private readonly ILogger<PaymentHub> _logger;
        private readonly IMapper _mapper;
        private readonly IAccountRepository _accountRepository;
        private readonly IConnectionService _connectionService;
        private readonly IPointOfSaleRepository _pointOfSaleRepository;

        public PaymentHub(IAccountRepository accountRepository, IMapper mapper, ILogger<PaymentHub> logger,
            IConnectionService connectionService, IPointOfSaleRepository pointOfSaleRepository)
        {
            _accountRepository = accountRepository;
            _logger = logger;
            _connectionService = connectionService;
            _pointOfSaleRepository = pointOfSaleRepository;
            _mapper = mapper;
        }

        [Authorize(Roles = "NfcClient,CashDesk")]
        public async Task JoinPointOfSale(PointOfSaleDevice device)
        {
            _logger.LogInformation("Client tries to connect to PoS " + device.PosId + " as " + device.Type);
            var pos = await _pointOfSaleRepository.GetPointOfSaleByIdAsync(device.PosId);
            if (pos == null)
            {
                _logger.LogError($"The PointOfSale {device.PosId} a client tries to connect to is unknown!");
                var e = new ProblemDetails
                {
                    Type = "signalr/join-point-of-sale",
                    Title = "PoS unknown",
                    Detail = "The Point of Sale you are trying to join is unknown!",
                    Status = StatusCodes.Status400BadRequest
                };
                await Clients.Caller.Error(e);
                return;
            }

            try
            {
                if (!_connectionService.AddConnection(Context.ConnectionId, device.PosId, device.Type))
                {
                    _logger.LogError($"A client wants to join as {device.Type} but is already joined!");
                    return;
                }

                await Groups.AddToGroupAsync(Context.ConnectionId, device.PosId.ToString());

                var newPosInfo = await _pointOfSaleRepository.GetPointOfSaleByIdAsync(device.PosId);

                await Clients.Caller.Welcome(newPosInfo);

                switch (device.Type)
                {
                    case PosClientType.NfcReader:
                        await Clients.OthersInGroup(device.PosId.ToString()).NewCardReader(newPosInfo);
                        break;
                    case PosClientType.CashDesk:
                        await Clients.OthersInGroup(device.PosId.ToString()).NewCashDesk(newPosInfo);
                        var nfcConnection = _connectionService.GetConnection(device.PosId, PosClientType.NfcReader);
                        if (nfcConnection != null)
                        {
                            await Clients.Client(nfcConnection).GetCard();
                        }

                        break;
                    default:
                        _logger.LogError($"A client wants to connect as {device.Type} which is not allowed!");
                        return;
                }

                await Clients.Group(PosClientType.EventSubscriber.ToString()).NewEvent(
                    new SubscriptionNotificationPointOfSaleInfo
                    {
                        EventClass = SubscriptionEventClasses.PointOfSaleInfo,
                        EventType = SubscriptionEventTypes.PosDeviceJoined,
                        Content = newPosInfo
                    });

                await LogPosStatesAsync();
            }
            catch (NfcClientAlreadyRegisteredForPointOfSale e)
            {
                _logger.LogError(e, "Another NFC client is already registered for this PointOfSale.");
                throw new HubException("Another NFC client is already registered for this PointOfSale.", e);
            }
            catch (CashDeskClientAlreadyRegisteredForPointOfSale e)
            {
                _logger.LogError(e, "Another CashDesk client is already registered for this PointOfSale.");
                throw new HubException("Another CashDesk client is already registered for this PointOfSale.", e);
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task SubscribeEvents()
        {
            _logger.LogInformation($"Admin is connecting as {PosClientType.EventSubscriber}.");

            await Groups.AddToGroupAsync(Context.ConnectionId, PosClientType.EventSubscriber.ToString());
            await Clients.Group(PosClientType.EventSubscriber.ToString()).NewEventSubscriber();
        }

        [Authorize(Roles = "NfcClient")]
        public async Task NewCard(Card card)
        {
            var account = await _accountRepository.GetAccountInformationByCardIdAsync(card.Id);
            var cardInfo = new CardInfo
            {
                Id = card.Id,
            };
            if (account != null)
            {
                var accountInfo = _mapper.Map<AccountInfo>(account);
                cardInfo.Account = accountInfo;
            }

            _logger.LogInformation("New card placed: " + cardInfo);

            await Clients.Group(_connectionService.GetPointOfSaleId(Context.ConnectionId, PosClientType.NfcReader)
                .ToString()).NewCard(cardInfo);
        }

        [Authorize(Roles = "NfcClient")]
        public async Task CurrentCard(Card card)
        {
            _logger.LogDebug("Currently placed card is: " + card);
            CardInfo cardInfo = null;
            var posIdNfcClient = _connectionService.GetPointOfSaleId(Context.ConnectionId, PosClientType.NfcReader);

            var account = await _accountRepository.GetAccountInformationByCardIdAsync(card.Id);

            var accountInfo = _mapper.Map<AccountInfo>(account);
            cardInfo = new CardInfo
            {
                Id = card.Id,
                Account = accountInfo
            };

            if (account == null)
            {
                if (card.Id != null)
                {
                    _logger.LogWarning("CurrentCard: The card placed is not associated with any account!");
                }

                await Clients.OthersInGroup(posIdNfcClient.ToString()).RemovedCard();
            }
            else
            {
                await Clients.OthersInGroup(posIdNfcClient.ToString()).NewCard(cardInfo);
            }

            _connectionService.PublishResponseFromNfc(Context.ConnectionId, cardInfo);
        }

        [Authorize(Roles = "NfcClient")]
        public async Task RemovedCard()
        {
            _logger.LogDebug("Card was removed.");
            await Clients
                .OthersInGroup(_connectionService.GetPointOfSaleId(Context.ConnectionId, PosClientType.NfcReader)
                    .ToString())
                .RemovedCard();
        }

        public override async Task OnConnectedAsync()
        {
            await Clients.Caller.JoinRequired("Connected to PaymentBackend. Please join a PointOfSale!");
            await base.OnConnectedAsync();
            await LogPosStatesAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var valueTuple = _connectionService.RemoveConnection(Context.ConnectionId);
            if (valueTuple.posId != Guid.Empty)
            {
                await Clients.OthersInGroup(valueTuple.posId.ToString()).DeviceLeftPos(new PointOfSaleDevice
                {
                    PosId = valueTuple.posId,
                    Type = valueTuple.clientType
                });
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, valueTuple.posId.ToString());

                await Clients.Group(PosClientType.EventSubscriber.ToString()).NewEvent(
                    new SubscriptionNotificationPointOfSaleInfo
                    {
                        EventClass = SubscriptionEventClasses.PointOfSaleInfo,
                        EventType = SubscriptionEventTypes.PosDeviceLeft,
                        Content = await _pointOfSaleRepository.GetPointOfSaleByIdAsync(valueTuple.posId)
                    });
            }
            else
            {
                // seems to be an GeneralSubscriber, which is not registered in _connectionService
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, PosClientType.EventSubscriber.ToString());
            }

            await base.OnDisconnectedAsync(exception);
            await LogPosStatesAsync();
        }

        private async Task LogPosStatesAsync()
        {
            _logger.LogInformation("Currently fully available PoS: " +
                                   (await _pointOfSaleRepository.GetPointsOfSaleAsync(new SieveModel
                                       {PageSize = int.MaxValue})).Results
                                   .Count(pos => pos.IsReady));
            _logger.LogInformation("Currently uncompleted PoS: " +
                                   (await _pointOfSaleRepository.GetPointsOfSaleAsync(new SieveModel
                                       {PageSize = int.MaxValue})).Results
                                   .Count(pos => !pos.IsReady));
        }
    }
}