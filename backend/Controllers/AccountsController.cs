﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using PaymentBackend.Models;
using PaymentBackend.Models.Validations;
using PaymentBackend.Services;
using Sieve.Models;

namespace PaymentBackend.Controllers
{
    [Authorize(Roles = "Admin")]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IMapper _mapper;
        private readonly IAuthenticationService _authenticationService;
        private readonly ITransactionRepository _transactionRepository;

        public AccountsController(IAccountRepository accountRepository,
            IMapper mapper,
            IAuthenticationService authenticationService,
            ITransactionRepository transactionRepository)
        {
            _accountRepository = accountRepository;
            _mapper = mapper;
            _authenticationService = authenticationService;
            _transactionRepository = transactionRepository;
        }

        // GET api/v1/accounts
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<AccountInfo>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<AccountInfo>>> GetAccounts([FromQuery] SieveModel sieveModel)
        {
            var result = await _accountRepository.GetAccounts(sieveModel);
            return Ok(_mapper.Map<PagedResult<AccountInfo>>(result));
        }

        // POST api/v1/accounts
        [HttpPost]
        [ProducesResponseType(typeof(AccountInfo), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<AccountInfo>> CreateAccount([FromBody] CreateAccount account)
        {
            var acc = _mapper.Map<Account>(account);
            acc.Id = Guid.NewGuid();
            var accountWithId = await _accountRepository.CreateAccountAsync(acc);
            return CreatedAtAction(nameof(GetAccounts), new {filters = $"id=={acc.Id}"},
                _mapper.Map<AccountInfo>(accountWithId));
        }

        // PUT api/v1/accounts/{id}
        [HttpPut("{id:guid}")]
        [Authorize(Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<AccountInfo>> UpdateAccount([FromBody] UpdateAccount updateAccount,
            [FromRoute] [IsValidAccountId] Guid id)
        {
            var account = _mapper.Map<Account>(updateAccount);
            account.Id = id;

            var accountUpdated = await _accountRepository.UpdateAccountAsync(account);

            return Ok(_mapper.Map<AccountInfo>(accountUpdated));
        }

        // PATCH api/v1/accounts/{id}/password
        [HttpPatch("{id:guid}/password")]
        [ProducesResponseType(typeof(AccountInfo), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdatePassword([FromBody] UpdateAccountPassword updateAccountPassword,
            [FromRoute] [IsValidAccountId] Guid id)
        {
            var account = _mapper.Map<Account>(updateAccountPassword);
            account.Id = id;

            var accountUpdated = await _accountRepository.UpdatePasswordAsync(account);

            return Ok(_mapper.Map<AccountInfo>(accountUpdated));
        }

        // PATCH api/v1/accounts/{id}/lock
        [HttpPatch("{id:guid}/lock")]
        [ProducesResponseType(typeof(AccountInfo), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> LockAccount([FromRoute] [IsValidAccountId] Guid id)
        {
            var account = await _accountRepository.GetAccountInformationByIdAsync(id);
            if (account.Locked)
            {
                return Conflict();
            }

            account.Locked = true;
            var acc = await _accountRepository.UpdateAccountAsync(account);

            return Ok(_mapper.Map<AccountInfo>(acc));
        }

        // PATCH api/v1/accounts/{id}/unlock
        [HttpPatch("{id:guid}/unlock")]
        [ProducesResponseType(typeof(AccountInfo), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> UnlockAccount([FromRoute] [IsValidAccountId] Guid id)
        {
            var account = await _accountRepository.GetAccountInformationByIdAsync(id);
            if (!account.Locked)
            {
                return Conflict();
            }

            account.Locked = false;
            var acc = await _accountRepository.UpdateAccountAsync(account);

            return Ok(_mapper.Map<AccountInfo>(acc));
        }

        // DELETE api/v1/accounts/{id}
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status423Locked)]
        public async Task<ActionResult> DeleteAccount([FromRoute] [IsValidAccountId] Guid id)
        {
            var account = await _accountRepository.GetAccountInformationByIdAsync(id);

            if (account.Roles.Any(r => r.Role.Name == "Admin"))
            {
                var errorMsg = new ProblemDetails
                {
                    Title = "Admin-User",
                    Detail = "You can't delete a admin. Change the role of the user before you delete him.",
                    Instance = HttpContext.Request.Path
                };
                return StatusCode(StatusCodes.Status423Locked, errorMsg);
            }

            var accountSieveFilter = new SieveModel {Filters = $"account.id=={id}"};
            var transactions = await _transactionRepository.GetTransactionsAsync(accountSieveFilter);
            
            if (transactions.Results.Any( t => t.GetType() == typeof(PurchaseTransaction)))
            {
                var errorMsg = new ProblemDetails
                {
                    Type = "payment/active-user-transactions",
                    Title = "Active User",
                    Detail = "You can't delete a user who has (already) some transactions in his history.",
                    Instance = HttpContext.Request.Path
                };
                return StatusCode(StatusCodes.Status423Locked, errorMsg);
            } 
            
            if (account.Balance > 0)
            {
                var errorMsg = new ProblemDetails
                {
                    Type = "payment/active-user-balance",
                    Title = "Active User",
                    Detail = "You can't delete a user who has still some money on his account.",
                    Instance = HttpContext.Request.Path
                };
                return StatusCode(StatusCodes.Status423Locked, errorMsg);
            }

            await _accountRepository.DeleteAccount(account);

            return Ok();
        }
    }
}