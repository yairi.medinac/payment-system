using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using PaymentBackend.Models;
using PaymentBackend.Models.Validations;
using PaymentBackend.Services;
using Sieve.Models;

namespace PaymentBackend.Controllers
{
    [Authorize(Roles = "Admin,CashDesk")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductTypesController : ControllerBase
    {
        private readonly IProductTypeRepository _productTypeRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductTypesController> _logger;

        public ProductTypesController(IProductTypeRepository productTypeRepository, IMapper mapper,
            ILogger<ProductTypesController> logger)
        {
            _productTypeRepository = productTypeRepository;
            _mapper = mapper;
            _logger = logger;
        }

        // GET api/producttypes
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ProductType>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ProductType>>> GetProductTypes([FromQuery] SieveModel sieveModel)
        {
            var productTypesResult = await _productTypeRepository.GetProductTypesAsync(sieveModel);

            return Ok(_mapper.Map<PagedResult<ProductTypeResponse>>(productTypesResult));
        }

        // GET api/v1/producttypes/{id}/image
        [HttpGet("{id:guid}/image")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> GetProductImage([FromRoute] [IsValidProductTypeId] Guid id)
        {
            var productType = await _productTypeRepository.GetProductTypeByIdAsync(id);

            var match = Regex.Match(productType.Image, @"data:(?<type>.+?);base64,(?<data>.+)");
            if (!match.Success)
            {
                return NotFound();
            }

            var base64Data = match.Groups["data"].Value;
            var mimeType = match.Groups["type"].Value;
            var binData = Convert.FromBase64String(base64Data);

            return File(binData, mimeType);
        }

        // POST api/v1/producttypes
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ProductResponse>> CreateProductType(
            [FromBody] CreateProductType createProductType)
        {
            var prod = _mapper.Map<ProductType>(createProductType);
            prod.Id = Guid.NewGuid();

            var newProductType = await _productTypeRepository.CreateProductTypeAsync(prod);

            return CreatedAtAction(nameof(GetProductTypes), new {filters = $"id=={newProductType.Id}"},
                newProductType);
        }

        // PUT api/v1/products/{id}
        [HttpPut("{id:guid}")]
        [Authorize(Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ProductType>> UpdateProductType([FromBody] UpdateProductType updateProductType,
            [FromRoute] [IsValidProductTypeId] Guid id)
        {
            var prod = _mapper.Map<ProductType>(updateProductType);
            prod.Id = id;
            var pt = await _productTypeRepository.UpdateProductTypeAsync(prod);
            return Ok(pt);
        }
    }
}