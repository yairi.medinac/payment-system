﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using PaymentBackend.Models;
using PaymentBackend.Models.Validations;
using PaymentBackend.Services;
using Sieve.Models;

namespace PaymentBackend.Controllers
{
    [Authorize(Roles = "Admin,CashDesk")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductsController> _logger;

        public ProductsController(IProductRepository productRepository, IMapper mapper,
            ILogger<ProductsController> logger)
        {
            _productRepository = productRepository;
            _mapper = mapper;
            _logger = logger;
        }

        // GET api/v1/products
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ProductResponse>>> GetProducts([FromQuery] SieveModel sieveModel)
        {
            var productsResult = await _productRepository.GetProductsAsync(sieveModel);

            return Ok(_mapper.Map<PagedResult<ProductResponse>>(productsResult));
        }

        // GET api/v1/products/{id}/image
        [HttpGet("{id:guid}/image")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> GetProductImage([FromRoute] [IsValidProductId] Guid id)
        {
            var product = await _productRepository.GetProductByIdAsync(id);

            var match = Regex.Match(product.Image, @"data:(?<type>.+?);base64,(?<data>.+)");
            if (!match.Success)
            {
                return NotFound();
            }

            var base64Data = match.Groups["data"].Value;
            var mimeType = match.Groups["type"].Value;
            var binData = Convert.FromBase64String(base64Data);

            return File(binData, mimeType);
        }

        // POST api/v1/products
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ProductResponse>> CreateProduct([FromBody] CreateProduct createProduct)
        {
            var prod = _mapper.Map<Product>(createProduct);
            prod.Id = Guid.NewGuid();

            var newProduct = await _productRepository.CreateProductAsync(prod);

            return CreatedAtAction(nameof(GetProducts), new {filters = $"id=={newProduct.Id}"},
                _mapper.Map<ProductResponse>(newProduct));
        }

        // PUT api/v1/products/{id}
        [HttpPut("{id:guid}")]
        [Authorize(Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ProductResponse>> UpdateProduct([FromBody] UpdateProduct updateProduct,
            [FromRoute] [IsValidProductId] Guid id)
        {
            var prod = _mapper.Map<Product>(updateProduct);
            prod.Id = id;

            var p = await _productRepository.UpdateProductAsync(prod);

            return Ok(_mapper.Map<ProductResponse>(p));
        }
    }
}