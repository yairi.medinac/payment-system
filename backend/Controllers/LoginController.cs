﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PaymentBackend.Models;
using PaymentBackend.Services;

namespace PaymentBackend.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IMapper _mapper;
        private readonly IAuthenticationService _authenticationService;

        public LoginController(IAccountRepository accountRepository, IMapper mapper,
            IAuthenticationService authenticationService)
        {
            _accountRepository = accountRepository;
            _mapper = mapper;
            _authenticationService = authenticationService;
        }

        /// <summary>
        /// Return a LoginResponse containing a Token.
        /// </summary>
        /// <param name="request"></param>
        /// <response code="403">Authentication credentials are unknown or incorrect.</response>      
        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(typeof(LoginResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var token = _authenticationService.Authenticate(
                await _accountRepository.GetAccountByUsernameAsync(request.Username),
                request.Password
            );

            if (token == null)
                return Forbid();

            return Ok(new LoginResponse {Token = token});
        }
    }
}