using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using PaymentBackend.Entities;
using PaymentBackend.Hubs;
using PaymentBackend.Models;
using PaymentBackend.Models.Hub;
using PaymentBackend.Models.Validations;
using PaymentBackend.Services;
using Sieve.Models;

namespace PaymentBackend.Controllers
{
    [Authorize(Roles = "Admin,CashDesk")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PointsOfSaleController : ControllerBase
    {
        private readonly IPointOfSaleRepository _pointOfSaleRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<PointsOfSaleController> _logger;
        private readonly IHubContext<PaymentHub, IPaymentHub> _paymentHubContext;

        public PointsOfSaleController(IPointOfSaleRepository pointOfSaleRepository, IMapper mapper,
            ILogger<PointsOfSaleController> logger, IHubContext<PaymentHub, IPaymentHub> paymentHubContext)
        {
            _pointOfSaleRepository = pointOfSaleRepository;
            _mapper = mapper;
            _logger = logger;
            _paymentHubContext = paymentHubContext;
        }

        // GET api/v1/PointsOfSale
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<PointOfSaleInfo>>> GetPointsOfSale([FromQuery] SieveModel sieveModel)
        {
            var pointOfSaleResult = await _pointOfSaleRepository.GetPointsOfSaleAsync(sieveModel);

            return Ok(pointOfSaleResult);
        }

        // POST api/v1/PointsOfSale
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<PointOfSaleInfo>> CreatePointOfSale(
            [FromBody] CreatePointOfSale createPointOfSale)
        {
            var pointOfSale = _mapper.Map<PointOfSale>(createPointOfSale);
            pointOfSale.Id = Guid.NewGuid();

            var newPos = await _pointOfSaleRepository.CreatePointOfSaleAsync(pointOfSale);

            await _paymentHubContext.Clients.Group(PosClientType.EventSubscriber.ToString()).NewEvent(
                new SubscriptionNotificationPointOfSaleInfo
                {
                    EventClass = SubscriptionEventClasses.PointOfSaleInfo,
                    EventType = SubscriptionEventTypes.PosAdded,
                    Content = newPos
                });

            return CreatedAtAction(nameof(GetPointsOfSale), new {filters = $"id=={newPos.Id}"}, newPos);
        }

        // PUT api/v1/PointsOfSale/{id}
        [HttpPut("{id:guid}")]
        [Authorize(Roles = "Admin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<PointOfSaleInfo>> UpdatePointOfSale(
            [FromBody] UpdatePointOfSale updatePointOfSale, [FromRoute] [IsValidPointOfSaleId] Guid id)
        {
            var pointOfSale = _mapper.Map<PointOfSale>(updatePointOfSale);
            pointOfSale.Id = id;

            var pos = await _pointOfSaleRepository.UpdatePointOfSaleAsync(pointOfSale);

            await _paymentHubContext.Clients.Group(PosClientType.EventSubscriber.ToString()).NewEvent(
                new SubscriptionNotificationPointOfSaleInfo
                {
                    EventClass = SubscriptionEventClasses.PointOfSaleInfo,
                    EventType = SubscriptionEventTypes.PosConfigured,
                    Content = pos
                });

            return Ok(pos);
        }
    }
}