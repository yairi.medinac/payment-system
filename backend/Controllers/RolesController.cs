using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PaymentBackend.Entities;
using PaymentBackend.Services;
using Sieve.Models;

namespace PaymentBackend.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ILogger<RolesController> _logger;
        private readonly IRolesRepository _rolesRepository;

        public RolesController(IMapper mapper, ILogger<RolesController> logger, IRolesRepository rolesRepository)
        {
            _mapper = mapper;
            _logger = logger;
            _rolesRepository = rolesRepository;
        }

        // GET api/roles
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Role>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Role>>> GetRoles([FromQuery] SieveModel sieveModel)
        {
            var rolesResult = await _rolesRepository.GetRolesAsync(sieveModel);

            return Ok(rolesResult);
        }
    }
}