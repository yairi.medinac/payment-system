using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PaymentBackend.Context;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using Sieve.Models;
using Sieve.Services;

namespace PaymentBackend.Services
{
    public class ProductRepository : IProductRepository
    {
        private readonly PaymentContext _context;
        private readonly SieveOptions _sieveOptions;
        private readonly ISieveProcessor _sieveProcessor;
        private readonly ILogger<ProductRepository> _logger;

        public ProductRepository(PaymentContext context, IOptions<SieveOptions> sieveOptions,
            ILogger<ProductRepository> logger, ISieveProcessor sieveProcessor)
        {
            _context = context;
            _sieveOptions = sieveOptions.Value;
            _logger = logger;
            _sieveProcessor = sieveProcessor;
        }

        public async Task<PagedResult<Product>> GetProductsAsync(SieveModel sieveModel)
        {
            var result = _context.Products.AsNoTracking().Include(p => p.ProductType);
            var pr = await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
            return pr;
        }

        public async Task<Product> GetProductByIdAsync(Guid productId)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"id=={productId.ToString()}"
            };

            var result = await GetProductsAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<Product> CreateProductAsync(Product newProduct)
        {
            var p = _context.Products.Add(newProduct);
            await _context.SaveChangesAsync();
            return p.Entity;
        }

        public async Task<Product> UpdateProductAsync(Product updateProduct)
        {
            var p = _context.Products.Update(updateProduct);
            await _context.SaveChangesAsync();
            return await GetProductByIdAsync(p.Entity.Id);
        }

        public async Task<Product> GetProductByNameAsync(string product)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"name=={product}"
            };
            var result = await GetProductsAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }
    }
}