using System.Threading.Tasks;
using PaymentBackend.Entities;

namespace PaymentBackend.Services
{
    public interface IAuthenticationService
    {
        string Authenticate(Account account, string password);
        string GetPasswordHash(string password, string salt);
        string GetSomeSalt();
    }
}