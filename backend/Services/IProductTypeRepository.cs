using System;
using System.Threading.Tasks;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using Sieve.Models;

namespace PaymentBackend.Services
{
    public interface IProductTypeRepository
    {
        Task<PagedResult<ProductType>> GetProductTypesAsync(SieveModel sieveModel);
        Task<ProductType> GetProductTypeByIdAsync(Guid productTypeId);
        Task<ProductType> GetProductTypeByNameAsync(string productTypeName);
        Task<ProductType> CreateProductTypeAsync(ProductType createProductType);
        Task<ProductType> UpdateProductTypeAsync(ProductType updateProductType);
    }
}