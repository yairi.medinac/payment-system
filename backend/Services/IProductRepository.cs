using System;
using System.Threading.Tasks;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using Sieve.Models;

namespace PaymentBackend.Services
{
    public interface IProductRepository
    {
        Task<PagedResult<Product>> GetProductsAsync(SieveModel sieveModel);
        Task<Product> GetProductByIdAsync(Guid productId);
        Task<Product> CreateProductAsync(Product newProduct);

        Task<Product> UpdateProductAsync(Product updateProduct);
        Task<Product> GetProductByNameAsync(string product);
    }
}