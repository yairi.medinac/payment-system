using System;
using System.Threading.Tasks;
using PaymentBackend.Models;

namespace PaymentBackend.Services
{
    public interface IConnectionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="posId"></param>
        /// <param name="clientType"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException">Is thrown if there are multiple connections registered for one PointOfSale</exception>
        string GetConnection(Guid posId, PosClientType clientType);

        bool AddConnection(string connectionId, Guid posId, PosClientType clientType);

        (Guid posId, PosClientType clientType) RemoveConnection(string connection);

        Guid GetPointOfSaleId(string connection, PosClientType clientType);

        // NFC specific methods
        Task<CardInfo> GetCardAsync(Guid posId);
        void PublishResponseFromNfc(string connectionId, CardInfo cardInfo);
        bool PosIsReady(Guid posId);
    }
}