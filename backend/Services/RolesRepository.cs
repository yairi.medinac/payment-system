using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using PaymentBackend.Context;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using Sieve.Models;
using Sieve.Services;

namespace PaymentBackend.Services
{
    class RolesRepository : IRolesRepository
    {
        private readonly PaymentContext _context;
        private readonly ISieveProcessor _sieveProcessor;
        private readonly SieveOptions _sieveOptions;

        public RolesRepository(PaymentContext context, ISieveProcessor sieveProcessor,
            IOptions<SieveOptions> sieveOptions)
        {
            _context = context;
            _sieveProcessor = sieveProcessor;
            _sieveOptions = sieveOptions.Value;
        }

        public async Task<PagedResult<Role>> GetRolesAsync(SieveModel sieveModel)
        {
            var result = _context.Roles.AsNoTracking();
            return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
        }

        public async Task<Role> GetRoleByIdAsync(Guid roleId)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"id=={roleId.ToString()}"
            };

            var result = await GetRolesAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<Role> GetRoleByNameAsync(string roleName)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"name=={roleName}"
            };

            var result = await GetRolesAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }
    }
}