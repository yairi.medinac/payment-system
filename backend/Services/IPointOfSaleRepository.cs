using System;
using System.Threading.Tasks;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using PaymentBackend.Models;
using Sieve.Models;

namespace PaymentBackend.Services
{
    public interface IPointOfSaleRepository
    {
        Task<PagedResult<PointOfSaleInfo>> GetPointsOfSaleAsync(SieveModel sieveModel);
        Task<PointOfSaleInfo> GetPointOfSaleByIdAsync(Guid posId);
        Task<PointOfSaleInfo> GetPointOfSaleByNameAsync(string posName);
        Task<PointOfSaleInfo> CreatePointOfSaleAsync(PointOfSale newPointOfSale);
        Task<PointOfSaleInfo> UpdatePointOfSaleAsync(PointOfSale updatePointOfSale);
    }
}