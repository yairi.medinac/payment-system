using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using PaymentBackend.Context;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using Sieve.Models;
using Sieve.Services;

namespace PaymentBackend.Services
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly PaymentContext _context;
        private readonly ISieveProcessor _sieveProcessor;
        private readonly SieveOptions _sieveOptions;

        public TransactionRepository(PaymentContext context, ISieveProcessor sieveProcessor,
            IOptions<SieveOptions> sieveOptions)
        {
            _context = context;
            _sieveProcessor = sieveProcessor;
            _sieveOptions = sieveOptions.Value;
        }

        public async Task<PagedResult<Transaction>> GetTransactionsAsync(SieveModel sieveModel)
        {
            var result = _context
                .Transactions
                .AsNoTracking()
                .Include(t => t.PointOfSale)
                .Include(t => t.Account)
                .Include(t => (t as PurchaseTransaction).Entries)
                .ThenInclude(e => e.Product)
                .ThenInclude(p => p.ProductType);
            return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
        }

        public async Task<Transaction> GetTransactionByIdAsync(Guid id)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"id=={id.ToString()}"
            };

            var result = await GetTransactionsAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<bool> WriteNewTransactionAsync(Transaction transaction)
        {
            var dbTransaction = _context.Database.BeginTransaction();
            try
            {
                _context.Add(transaction);
                _context.SaveChanges();

                // Update Account Balance
                await RecalculateAccountBalance(transaction.AccountId);

                dbTransaction.Commit();
            }
            catch (DbUpdateException e)
            {
                Console.WriteLine(e);
                dbTransaction.Rollback();
                return false;
            }

            return true;
        }

        public async Task<bool> CancelTransactionAsync(Transaction transaction)
        {
            _context.Transactions.Attach(transaction);
            transaction.Cancelled = DateTime.Now;
            _context.Entry(transaction).Property(a => a.Cancelled).IsModified = true;
            var result = await _context.SaveChangesAsync();

            await RecalculateAccountBalance(transaction.AccountId);

            return result > 0;
        }

        private async Task RecalculateAccountBalance(Guid accountId)
        {
            var accountInfo = _context.Accounts.SingleOrDefault(account => account.Id == accountId);
            if (accountInfo == null) return;
            var newAccountBalance = _context.Transactions.Where(t => t.AccountId == accountInfo.Id && t.Cancelled == null).ToArray().Sum(
                t =>
                {
                    if (t is PurchaseTransaction)
                    {
                        return t.Sum * -1;
                    }

                    return t.Sum;
                });
            accountInfo.Balance = newAccountBalance;
            await _context.SaveChangesAsync();
        }
    }
}