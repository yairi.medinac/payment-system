using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using PaymentBackend.Context;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using Sieve.Models;
using Sieve.Services;

namespace PaymentBackend.Services
{
    public class AccountRepository : IAccountRepository
    {
        private readonly PaymentContext _context;
        private readonly ISieveProcessor _sieveProcessor;
        private readonly IAuthenticationService _authenticationService;
        private readonly SieveOptions _sieveOptions;

        public AccountRepository(PaymentContext context, ISieveProcessor sieveProcessor,
            IAuthenticationService authenticationService, IOptions<SieveOptions> sieveOptions)
        {
            _context = context;
            _sieveProcessor = sieveProcessor;
            _authenticationService = authenticationService;
            _sieveOptions = sieveOptions.Value;
        }

        public async Task<PagedResult<Account>> GetAccounts(SieveModel sieveModel)
        {
            var result = _context
                .Accounts
                .AsNoTracking()
                .Include(a => a.Roles).ThenInclude(r => r.Role);
            return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
        }

        public async Task<Account> GetAccountByUsernameAsync(string username)
        {
            return await _context
                .Accounts
                .AsNoTracking()
                .Include(a => a.Roles).ThenInclude(r => r.Role)
                .SingleOrDefaultAsync(a => a.Username == username);
        }

        public async Task<Account> GetAccountInformationByIdAsync(Guid id)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"id=={id.ToString()}"
            };

            var result = await GetAccounts(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<Account> GetAccountInformationByCardIdAsync(string cardId)
        {
            if (cardId == null)
            {
                return null;
            }

            var acc = await _context.Accounts
                .Include(a => a.Roles).ThenInclude(ur => ur.Role)
                .SingleOrDefaultAsync(account => account.CardId == cardId);

            return acc;
        }

        public async Task<Account> CreateAccountAsync(Account account)
        {
            account.PasswordSalt = _authenticationService.GetSomeSalt();
            account.Password = _authenticationService.GetPasswordHash(account.Password, account.PasswordSalt);

            _context.Accounts.Add(account);
            await _context.SaveChangesAsync();

            var acc = await GetAccountInformationByIdAsync(account.Id);
            return acc;
        }

        public async Task<Account> UpdateAccountAsync(Account account)
        {
            var acc = await _context.Accounts
                .Include(a => a.Roles).ThenInclude(ur => ur.Role)
                .Where(a => a.Id == account.Id)
                .SingleOrDefaultAsync();
            acc.Username = account.Username;
            acc.Locked = account.Locked;
            acc.CardId = account.CardId;
            acc.Overdraft = account.Overdraft;
            var arToRemove = acc.Roles.Where(r => account.Roles.All(r2 => r2.RoleId != r.RoleId)).ToList();
            foreach (var accountRole in arToRemove)
            {
                acc.Roles.Remove(accountRole);
            }

            var arToAdd = account.Roles.Where(r => acc.Roles.All(r2 => r2.RoleId != r.RoleId)).ToList();
            foreach (var accountRole in arToAdd)
            {
                acc.Roles.Add(accountRole);
            }

            await _context.SaveChangesAsync();
            return await GetAccountInformationByIdAsync(account.Id);
        }

        public async Task<Account> UpdatePasswordAsync(Account account)
        {
            // generate new password-hash
            account.PasswordSalt = _authenticationService.GetSomeSalt();
            account.Password = _authenticationService.GetPasswordHash(account.Password, account.PasswordSalt);

            _context.Accounts.Attach(account);
            _context.Entry(account).Property(a => a.Password).IsModified = true;
            _context.Entry(account).Property(a => a.PasswordSalt).IsModified = true;

            await _context.SaveChangesAsync();
            return await GetAccountInformationByIdAsync(account.Id);
        }

        public async Task<Boolean> DeleteAccount(Account account)
        {
            _context.Accounts.Remove(account);
            
            await _context.SaveChangesAsync();
            return true;
        }
    }
}