using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using PaymentBackend.Context;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using Sieve.Models;
using Sieve.Services;

namespace PaymentBackend.Services
{
    public class ProductTypeRepository : IProductTypeRepository
    {
        private readonly PaymentContext _context;
        private readonly ISieveProcessor _sieveProcessor;
        private readonly SieveOptions _sieveOptions;

        public ProductTypeRepository(PaymentContext context, ISieveProcessor sieveProcessor,
            IOptions<SieveOptions> sieveOptions)
        {
            _context = context;
            _sieveProcessor = sieveProcessor;
            _sieveOptions = sieveOptions.Value;
        }

        public async Task<PagedResult<ProductType>> GetProductTypesAsync(SieveModel sieveModel)
        {
            var result = _context.ProductTypes.AsNoTracking();
            return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
        }

        public async Task<ProductType> GetProductTypeByIdAsync(Guid productTypeId)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"id=={productTypeId.ToString()}"
            };

            var result = await GetProductTypesAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<ProductType> GetProductTypeByNameAsync(string productTypeName)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"name=={productTypeName}"
            };

            var result = await GetProductTypesAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<ProductType> CreateProductTypeAsync(ProductType newProductType)
        {
            var p = _context.ProductTypes.Add(newProductType);
            await _context.SaveChangesAsync();
            return p.Entity;
        }

        public async Task<ProductType> UpdateProductTypeAsync(ProductType updateProductType)
        {
            var p = _context.ProductTypes.Update(updateProductType);
            await _context.SaveChangesAsync();
            return p.Entity;
        }
    }
}