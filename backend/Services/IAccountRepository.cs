using System;
using System.Threading.Tasks;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using Sieve.Models;

namespace PaymentBackend.Services
{
    public interface IAccountRepository
    {
        Task<PagedResult<Account>> GetAccounts(SieveModel sieveModel);
        Task<Account> GetAccountByUsernameAsync(string username);
        Task<Account> GetAccountInformationByIdAsync(Guid id);
        Task<Account> GetAccountInformationByCardIdAsync(string cardId);
        Task<Account> CreateAccountAsync(Account account);
        Task<Account> UpdateAccountAsync(Account account);
        Task<Account> UpdatePasswordAsync(Account account);
        Task<Boolean> DeleteAccount(Account account);
    }
}