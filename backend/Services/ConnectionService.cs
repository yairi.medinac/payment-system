using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PaymentBackend.Exceptions;
using PaymentBackend.Hubs;
using PaymentBackend.Models;

namespace PaymentBackend.Services
{
    public class ConnectionService : IConnectionService
    {
        private readonly Dictionary<string, (Guid posId, PosClientType clientType)> _connections =
            new Dictionary<string, (Guid posId, PosClientType)>();

        private readonly ConcurrentDictionary<string, CardInfo> _nfcGetCardCalls =
            new ConcurrentDictionary<string, CardInfo>();

        private readonly IHubContext<PaymentHub, IPaymentHub> _paymentHubContext;

        public ConnectionService(IHubContext<PaymentHub, IPaymentHub> paymentHubContext)
        {
            _paymentHubContext = paymentHubContext;
        }

        public string GetConnection(Guid posId, PosClientType clientType)
        {
            return _connections
                .SingleOrDefault(c => c.Value.posId == posId && c.Value.clientType == clientType)
                .Key;
        }

        public bool AddConnection(string connectionId, Guid posId, PosClientType clientType)
        {
            if (clientType == PosClientType.EventSubscriber) return false;

            if (_connections.Count(c => c.Value.posId == posId && c.Value.clientType == clientType) > 0)
            {
                switch (clientType)
                {
                    case PosClientType.NfcReader:
                        throw new NfcClientAlreadyRegisteredForPointOfSale();
                    case PosClientType.CashDesk:
                        throw new CashDeskClientAlreadyRegisteredForPointOfSale();
                }
            }

            return _connections.TryAdd(connectionId, (posId, clientType));
        }

        public (Guid posId, PosClientType clientType) RemoveConnection(string connectionId)
        {
            if (_connections.TryGetValue(connectionId, out var valueTuple))
                _connections.Remove(connectionId);

            return valueTuple;
        }

        public Guid GetPointOfSaleId(string connection, PosClientType clientType)
        {
            return _connections
                .SingleOrDefault(c => c.Key == connection && c.Value.clientType == clientType)
                .Value
                .posId;
        }

        public async Task<CardInfo> GetCardAsync(Guid posId)
        {
            var connectionId = _connections
                .SingleOrDefault(c => c.Value.posId == posId && c.Value.clientType == PosClientType.NfcReader)
                .Key;

            if (connectionId == null)
            {
                // no nfc connection for this PoS this should be validated before here!
                return null;
            }

            if (!_nfcGetCardCalls.TryAdd((string) connectionId, null))
            {
                // already pending request!
                throw new NotImplementedException();
            }

            await _paymentHubContext.Clients.Client(connectionId).GetCard();

            // task erzeugen und call wegspeichern
            var ctSource = new CancellationTokenSource();
            var t = Task.Run(() =>
            {
                while (true)
                {
                    if (!_nfcGetCardCalls.TryGetValue(connectionId, out var cardInfo))
                    {
                        return null;
                    }

                    if (cardInfo != null)
                    {
                        return cardInfo;
                    }

                    Thread.Sleep(100);
                }
            }, ctSource.Token);

            CardInfo card = null;
            try
            {
                card = await TimeoutAfter(t, new TimeSpan(0, 0, 15));
            }
            catch (TimeoutException)
            {
                ctSource.Cancel();
            }

            _nfcGetCardCalls.TryRemove(connectionId, out _);

            return card;
        }

        public void PublishResponseFromNfc(string connectionId, CardInfo cardInfo)
        {
            if (!_nfcGetCardCalls.ContainsKey(connectionId))
            {
                //result was not expected!?
                return;
            }

            if (cardInfo == null)
            {
                _nfcGetCardCalls.TryRemove(connectionId, out _);
                return;
            }

            _nfcGetCardCalls[connectionId] = cardInfo;
        }

        public bool PosIsReady(Guid posId)
        {
            return _connections
                       .Count(c => c.Value.posId == posId &&
                                   (c.Value.clientType == PosClientType.NfcReader ||
                                    c.Value.clientType == PosClientType.CashDesk)
                       ) == 2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        /// <param name="timeout"></param>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        /// <exception cref="TimeoutException"></exception>
        private static async Task<TResult> TimeoutAfter<TResult>(Task<TResult> task, TimeSpan timeout)
        {
            using (var timeoutCancellationTokenSource = new CancellationTokenSource())
            {
                var completedTask = await Task.WhenAny(task, Task.Delay(timeout, timeoutCancellationTokenSource.Token));
                if (completedTask != task) throw new TimeoutException("The operation has timed out.");
                timeoutCancellationTokenSource.Cancel();
                return await task; // Very important in order to propagate exceptions
            }
        }
    }
}