using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using PaymentBackend.Context;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using PaymentBackend.Models;
using Sieve.Models;
using Sieve.Services;

namespace PaymentBackend.Services
{
    public class PointOfSaleRepository : IPointOfSaleRepository
    {
        private readonly PaymentContext _context;
        private readonly IConnectionService _connectionService;
        private readonly SieveOptions _sieveOptions;
        private readonly ISieveProcessor _sieveProcessor;

        public PointOfSaleRepository(PaymentContext context, ISieveProcessor sieveProcessor,
            IConnectionService connectionService, IOptions<SieveOptions> sieveOptions)
        {
            _context = context;
            _sieveProcessor = sieveProcessor;
            _connectionService = connectionService;
            _sieveOptions = sieveOptions.Value;
        }

        public async Task<PagedResult<PointOfSaleInfo>> GetPointsOfSaleAsync(SieveModel sieveModel)
        {
            var result = _context.PointsOfSale.AsNoTracking().Select(pos => new PointOfSaleInfo
            {
                Id = pos.Id,
                Name = pos.Name,
                HasNfcClient = _connectionService.GetConnection(pos.Id, PosClientType.NfcReader) != null,
                HasCashDeskClient = _connectionService.GetConnection(pos.Id, PosClientType.CashDesk) != null
            });
            return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
        }

        public async Task<PointOfSaleInfo> GetPointOfSaleByIdAsync(Guid posId)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"id=={posId.ToString()}"
            };

            var result = await GetPointsOfSaleAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<PointOfSaleInfo> GetPointOfSaleByNameAsync(string posName)
        {
            var sieveModel = new SieveModel
            {
                Filters = $"name=={posName}"
            };

            var result = await GetPointsOfSaleAsync(sieveModel);
            return result.Results.SingleOrDefault();
        }

        public async Task<PointOfSaleInfo> CreatePointOfSaleAsync(PointOfSale newPointOfSale)
        {
            _context.PointsOfSale.Add(newPointOfSale);
            await _context.SaveChangesAsync();
            return await GetPointOfSaleByIdAsync(newPointOfSale.Id);
        }

        public async Task<PointOfSaleInfo> UpdatePointOfSaleAsync(PointOfSale updatePointOfSale)
        {
            _context.PointsOfSale.Update(updatePointOfSale);
            await _context.SaveChangesAsync();
            return await GetPointOfSaleByIdAsync(updatePointOfSale.Id);
        }
    }
}