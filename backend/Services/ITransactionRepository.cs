using System;
using System.Threading.Tasks;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using Sieve.Models;

namespace PaymentBackend.Services
{
    public interface ITransactionRepository
    {
        Task<PagedResult<Transaction>> GetTransactionsAsync(SieveModel sieveModel);
        Task<Transaction> GetTransactionByIdAsync(Guid id);
        Task<bool> WriteNewTransactionAsync(Transaction transaction);
        Task<bool> CancelTransactionAsync(Transaction transaction);
    }
}