using System;
using System.Threading.Tasks;
using PaymentBackend.Entities;
using PaymentBackend.Helpers.Sieve;
using Sieve.Models;

namespace PaymentBackend.Services
{
    public interface IRolesRepository
    {
        Task<PagedResult<Role>> GetRolesAsync(SieveModel sieveModel);
        Task<Role> GetRoleByIdAsync(Guid roleId);
        Task<Role> GetRoleByNameAsync(string roleName);
    }
}