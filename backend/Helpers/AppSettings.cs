namespace PaymentBackend.Helpers
{
    public class AppSettings
    {
        public string JwtSecret { get; set; }
    }
}