using Microsoft.Extensions.Options;
using PaymentBackend.Entities;
using Sieve.Models;
using Sieve.Services;

namespace PaymentBackend.Helpers.Sieve
{
    public class ApplicationSieveProcessor : SieveProcessor
    {
        public ApplicationSieveProcessor(IOptions<SieveOptions> options, ISieveCustomSortMethods customSortMethods,
            ISieveCustomFilterMethods customFilterMethods)
            : base(options, customSortMethods, customFilterMethods)
        {
        }

        protected override SievePropertyMapper MapProperties(SievePropertyMapper mapper)
        {
            mapper.Property<Product>(p => p.ProductType.Name)
                .CanFilter()
                .CanSort();
            
            mapper.Property<Product>(p => p.ProductType.Locked)
                .CanFilter()
                .CanSort();

            mapper.Property<Transaction>(t => t.PointOfSale.Name)
                .CanFilter()
                .CanSort();

            mapper.Property<Transaction>(t => t.Account.Id)
                .CanFilter()
                .CanSort();
            
            mapper.Property<Transaction>(t => t.Account.Username)
                .CanFilter()
                .CanSort();

            return mapper;
        }
    }
}