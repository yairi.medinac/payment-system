using System.Linq;
using PaymentBackend.Entities;
using Sieve.Services;

namespace PaymentBackend.Helpers.Sieve
{
    public class SieveCustomFilterMethods : ISieveCustomFilterMethods
    {
        public IQueryable<Transaction> IsCancelled(IQueryable<Transaction> source, string op, string[] values)
        {
            return values[0] == "true" ? source.Where(t => t.Cancelled != null) : source.Where(t => t.Cancelled == null);
        }
    }
}