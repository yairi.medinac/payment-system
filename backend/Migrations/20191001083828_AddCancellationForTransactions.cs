﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PaymentBackend.Migrations
{
    public partial class AddCancellationForTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Cancelled",
                table: "Transactions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cancelled",
                table: "Transactions");
        }
    }
}
