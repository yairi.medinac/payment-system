﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PaymentBackend.Migrations
{
    public partial class AddOverdraftToAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Overdraft",
                table: "Accounts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Overdraft",
                table: "Accounts");
        }
    }
}
