﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PaymentBackend.Migrations
{
    public partial class AddFkWithCascadeDenyForPosToTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_PointsOfSale_PointOfSaleId",
                table: "Transactions");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_PointsOfSale_PointOfSaleId",
                table: "Transactions",
                column: "PointOfSaleId",
                principalTable: "PointsOfSale",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_PointsOfSale_PointOfSaleId",
                table: "Transactions");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_PointsOfSale_PointOfSaleId",
                table: "Transactions",
                column: "PointOfSaleId",
                principalTable: "PointsOfSale",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
