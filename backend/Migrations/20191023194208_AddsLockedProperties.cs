﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PaymentBackend.Migrations
{
    public partial class AddsLockedProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Locked",
                table: "ProductTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Locked",
                table: "Products",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Locked",
                table: "ProductTypes");

            migrationBuilder.DropColumn(
                name: "Locked",
                table: "Products");
        }
    }
}
