﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PaymentBackend.Migrations
{
    public partial class AddsOrderPropertyToProductType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "ProductTypes",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Order",
                table: "ProductTypes");
        }
    }
}
