﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PaymentBackend.Migrations
{
    public partial class AddDiscountColumnToRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DiscountPercent",
                table: "Roles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "DiscountPercent", "Name" },
                values: new object[] { new Guid("28376b8e-9a34-41a3-ba69-3732902b9fe4"), 10, "Member" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: new Guid("28376b8e-9a34-41a3-ba69-3732902b9fe4"));

            migrationBuilder.DropColumn(
                name: "DiscountPercent",
                table: "Roles");
        }
    }
}
