using System;

namespace PaymentBackend.Exceptions
{
    public class NfcClientAlreadyRegisteredForPointOfSale : Exception
    {
        public NfcClientAlreadyRegisteredForPointOfSale()
        {
        }

        public NfcClientAlreadyRegisteredForPointOfSale(string message) : base(message)
        {
        }

        public NfcClientAlreadyRegisteredForPointOfSale(string message, Exception inner) : base(message, inner)
        {
        }
    }
}