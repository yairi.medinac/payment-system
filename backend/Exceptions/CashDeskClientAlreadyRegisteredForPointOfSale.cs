using System;

namespace PaymentBackend.Exceptions
{
    public class CashDeskClientAlreadyRegisteredForPointOfSale : Exception
    {
        public CashDeskClientAlreadyRegisteredForPointOfSale()
        {
        }

        public CashDeskClientAlreadyRegisteredForPointOfSale(string message) : base(message)
        {
        }

        public CashDeskClientAlreadyRegisteredForPointOfSale(string message, Exception inner) : base(message, inner)
        {
        }
    }
}