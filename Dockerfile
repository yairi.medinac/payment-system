FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine AS runtime
WORKDIR /app
COPY ./backend/out .

COPY ./frontend/dist/cashdesk wwwroot/cashdesk
COPY ./frontend/dist/admin wwwroot/admin

EXPOSE 5000
EXPOSE 5001

ENTRYPOINT ["dotnet", "backend.dll"]
