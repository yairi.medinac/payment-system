# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.5](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.4...1.0.5) (2019-11-02)


### Bug Fixes

* **REST:** fixes issue with base paths for frontends ([c2b4be9](https://gitlab.com/homescreen.rocks/payment-system/commit/c2b4be9))

### [1.0.4](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.3...1.0.4) (2019-11-02)


### Bug Fixes

* **cashdesk:** fixes issue with zero prices in Cart ([6345793](https://gitlab.com/homescreen.rocks/payment-system/commit/6345793))

### [1.0.3](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.2...1.0.3) (2019-11-02)


### Bug Fixes

* **rest:** refactores images to URLs instead of full payload on every request ([93ebd0f](https://gitlab.com/homescreen.rocks/payment-system/commit/93ebd0f))

### [1.0.2](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.1...1.0.2) (2019-11-01)


### Bug Fixes

* **Cashdesk:** show description of no product image exists ([d6cc585](https://gitlab.com/homescreen.rocks/payment-system/commit/d6cc58522ee420398d8a51b71767f00e0e20f247))

### [1.0.1](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0...1.0.1) (2019-11-01)


### Bug Fixes

* **Cashdesk:** fix initial loading of all products ([35cd047](https://gitlab.com/homescreen.rocks/payment-system/commit/35cd0476495de9b826033335a6ae44447a6a81f6))

## [1.0.0](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.5...1.0.0) (2019-10-31)


### Bug Fixes

* **TouchEvents:** Fixes scrolling and swipe events for safari and chrome ([b4e1fac](https://gitlab.com/homescreen.rocks/payment-system/commit/b4e1facfec71109392f02f081bb44356f5e3bfe5)), closes [#80](https://gitlab.com/homescreen.rocks/payment-system/issues/80)

## [1.0.0-rc.5](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.4...1.0.0-rc.5) (2019-10-31)


### Features

* **cashdesk:** adds warnings if NFC client is not ready ([9d8f3a9](https://gitlab.com/homescreen.rocks/payment-system/commit/9d8f3a92b0f54bec99ecc9c3d26114104f7c6c6e)), closes [#97](https://gitlab.com/homescreen.rocks/payment-system/issues/97)
* **REST:** adds lock routes for products and product types ([849f513](https://gitlab.com/homescreen.rocks/payment-system/commit/849f513c0e7b9d7907744a05e3443a539f72e98f))
* **wizard:** polished the wizard stepper ([8aee933](https://gitlab.com/homescreen.rocks/payment-system/commit/8aee9332399f2e1d29e6e10c4aaa08f00a87d5a6)), closes [#112](https://gitlab.com/homescreen.rocks/payment-system/issues/112)


### Bug Fixes

* **AccountList:** navigate to account list after generating new user ([0c2acab](https://gitlab.com/homescreen.rocks/payment-system/commit/0c2acabab9b7900dccfda904442a2c066a7bf79e)), closes [#116](https://gitlab.com/homescreen.rocks/payment-system/issues/116)
* **adminui:** fixes AOT type mismatch error ([855f50e](https://gitlab.com/homescreen.rocks/payment-system/commit/855f50efcc5a85094a498ce48a26b288869eb8bd)), closes [#83](https://gitlab.com/homescreen.rocks/payment-system/issues/83)
* **cashdesk:** reload cardInfo after cancelling a transaction ([bb90785](https://gitlab.com/homescreen.rocks/payment-system/commit/bb90785d3543037f0c4a2e21e30b7f38b89531bd)), closes [#111](https://gitlab.com/homescreen.rocks/payment-system/issues/111)
* **cashdesk:** reload products on every unlock ([8a201ee](https://gitlab.com/homescreen.rocks/payment-system/commit/8a201eedb25b71e8e2c7ce4641d3d8b36d8dd0a1)), closes [#114](https://gitlab.com/homescreen.rocks/payment-system/issues/114)
* **transactionresponses:** fixes AOT warnings ([88cae6f](https://gitlab.com/homescreen.rocks/payment-system/commit/88cae6f2864eb08340818e032119ca142ef0e2ba))

## [1.0.0-rc.4](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.3...1.0.0-rc.4) (2019-10-18)


### Bug Fixes

* **cashdesk:** adds more color to checkout button to get more visual focus ([3f79064](https://gitlab.com/homescreen.rocks/payment-system/commit/3f79064)), closes [#106](https://gitlab.com/homescreen.rocks/payment-system/issues/106)
* **cashdesk:** fixes charging sidebar ([fca13e0](https://gitlab.com/homescreen.rocks/payment-system/commit/fca13e0)), closes [#77](https://gitlab.com/homescreen.rocks/payment-system/issues/77)
* **cashdesk:** fixes sorting of products by `order` property ([fa45b50](https://gitlab.com/homescreen.rocks/payment-system/commit/fa45b50)), closes [#109](https://gitlab.com/homescreen.rocks/payment-system/issues/109)
* **Cashdesk:** chown member discount on products ([6e19404](https://gitlab.com/homescreen.rocks/payment-system/commit/6e19404))
* **Cashdesk:** update styling of card ([d1fb028](https://gitlab.com/homescreen.rocks/payment-system/commit/d1fb028)), closes [#107](https://gitlab.com/homescreen.rocks/payment-system/issues/107)
* **Cashdesk:** updates discount logic in products and cart ([cab4904](https://gitlab.com/homescreen.rocks/payment-system/commit/cab4904)), closes [#103](https://gitlab.com/homescreen.rocks/payment-system/issues/103)
* **fix:** fix ([1a1127f](https://gitlab.com/homescreen.rocks/payment-system/commit/1a1127f))
* **webserver:** fixes delivery of .webmanifest files by Kestrel ([5179d16](https://gitlab.com/homescreen.rocks/payment-system/commit/5179d16)), closes [#110](https://gitlab.com/homescreen.rocks/payment-system/issues/110)
* **wizard:** refactors card assignment step ([22d7dea](https://gitlab.com/homescreen.rocks/payment-system/commit/22d7dea)), closes [#101](https://gitlab.com/homescreen.rocks/payment-system/issues/101)
* **wizard:** some minor code improvements in PoS handling in Wizard ([1786440](https://gitlab.com/homescreen.rocks/payment-system/commit/1786440))


### Features

* **favicon:** adds favicon ([c4e042a](https://gitlab.com/homescreen.rocks/payment-system/commit/c4e042a)), closes [#74](https://gitlab.com/homescreen.rocks/payment-system/issues/74)
* **wizard:** adds charging accounts in step 3 ([88b6e97](https://gitlab.com/homescreen.rocks/payment-system/commit/88b6e97)), closes [#91](https://gitlab.com/homescreen.rocks/payment-system/issues/91)

## [1.0.0-rc.3](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.2...1.0.0-rc.3) (2019-10-11)


### Bug Fixes

* **AdminUi:** update code to latest version ([216591e](https://gitlab.com/homescreen.rocks/payment-system/commit/216591e))
* **Checkout:** reset checkout sum ([2150309](https://gitlab.com/homescreen.rocks/payment-system/commit/2150309))
* **fix:** fix ([cc6da1c](https://gitlab.com/homescreen.rocks/payment-system/commit/cc6da1c))
* **OrderList:** disable cancelled checkboxes in orders overview ([76f61f5](https://gitlab.com/homescreen.rocks/payment-system/commit/76f61f5)), closes [#95](https://gitlab.com/homescreen.rocks/payment-system/issues/95)
* **UserForm:** setting a new password works again ([41a68e1](https://gitlab.com/homescreen.rocks/payment-system/commit/41a68e1)), closes [#92](https://gitlab.com/homescreen.rocks/payment-system/issues/92)


### Features

* **AccountList:** disable locked checkboxes in account overview ([f8d9249](https://gitlab.com/homescreen.rocks/payment-system/commit/f8d9249))
* **Monitoring:** implementing a monitoringscreen to see all transactions ([08430fa](https://gitlab.com/homescreen.rocks/payment-system/commit/08430fa))
* **producttype:** adds `order` property to ProductTypes ([ee214b7](https://gitlab.com/homescreen.rocks/payment-system/commit/ee214b7)), closes [#85](https://gitlab.com/homescreen.rocks/payment-system/issues/85)
* **registrationwizard:** adds an wizard feature for creating and configuration accounts ([31674cb](https://gitlab.com/homescreen.rocks/payment-system/commit/31674cb)), closes [#67](https://gitlab.com/homescreen.rocks/payment-system/issues/67)

## [1.0.0-rc.2](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.1...1.0.0-rc.2) (2019-10-06)


### Bug Fixes

* **ClientEmulator:** nfcclient loops through newcard and removecard ([b55683f](https://gitlab.com/homescreen.rocks/payment-system/commit/b55683f))
* **Docker:** build backend in ci ([01a8aa8](https://gitlab.com/homescreen.rocks/payment-system/commit/01a8aa8)), closes [#69](https://gitlab.com/homescreen.rocks/payment-system/issues/69)
* **Docker:** fix image name ([a1b08a1](https://gitlab.com/homescreen.rocks/payment-system/commit/a1b08a1))
* **Migration:** add ENV toggle for migration execution ([0c0e57c](https://gitlab.com/homescreen.rocks/payment-system/commit/0c0e57c)), closes [#68](https://gitlab.com/homescreen.rocks/payment-system/issues/68)


### Features

* **Accounts:** adds a configurable overdraft facility per account ([0653fd6](https://gitlab.com/homescreen.rocks/payment-system/commit/0653fd6)), closes [#76](https://gitlab.com/homescreen.rocks/payment-system/issues/76)
* **Charge:** allow negative amounts for cash out purpose ([0b24d9d](https://gitlab.com/homescreen.rocks/payment-system/commit/0b24d9d))
* **Product:** adds `order` property to Product ([b9fca0b](https://gitlab.com/homescreen.rocks/payment-system/commit/b9fca0b)), closes [#72](https://gitlab.com/homescreen.rocks/payment-system/issues/72)
* **Roles:** adds `member` role with predefined discount of 10% ([804f49b](https://gitlab.com/homescreen.rocks/payment-system/commit/804f49b)), closes [#48](https://gitlab.com/homescreen.rocks/payment-system/issues/48)
* **Transactions:** adds possibility to cancel a Transaction ([29dc4f5](https://gitlab.com/homescreen.rocks/payment-system/commit/29dc4f5)), closes [#71](https://gitlab.com/homescreen.rocks/payment-system/issues/71)

## [1.0.0-rc.1](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-rc.0...1.0.0-rc.1) (2019-09-26)


### Bug Fixes

* **Migrations:** remove ef image an integrate migrate into startup ([308ebde](https://gitlab.com/homescreen.rocks/payment-system/commit/308ebde)), closes [#61](https://gitlab.com/homescreen.rocks/payment-system/issues/61)



## [1.0.0-rc.0](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-beta.4...1.0.0-rc.0) (2019-09-17)


### Bug Fixes

* **Account:** validates cardId for usage in other accounts ([c0f3637](https://gitlab.com/homescreen.rocks/payment-system/commit/c0f3637)), closes [#60](https://gitlab.com/homescreen.rocks/payment-system/issues/60)
* **Charge:** do not allow to charge 0 EUR ([925a731](https://gitlab.com/homescreen.rocks/payment-system/commit/925a731))
* **DB:** fixes connection string to connect to MariaDB ([29d53db](https://gitlab.com/homescreen.rocks/payment-system/commit/29d53db))
* **DockerFiles:** fixing alignments ([114998e](https://gitlab.com/homescreen.rocks/payment-system/commit/114998e))
* **Filter:** also allow filtering for account Id ([c3e1112](https://gitlab.com/homescreen.rocks/payment-system/commit/c3e1112))
* **Filter:** change account filter from id to name ([f4a291a](https://gitlab.com/homescreen.rocks/payment-system/commit/f4a291a))
* **Filter:** fix pagination ([8dcbf77](https://gitlab.com/homescreen.rocks/payment-system/commit/8dcbf77))
* **Filter:** update filter/sort configuration ([23c2dd7](https://gitlab.com/homescreen.rocks/payment-system/commit/23c2dd7))

## [1.0.0-beta.4](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-beta.3...1.0.0-beta.4) (2019-07-08)


### Bug Fixes

* **NfcClient:** remove console inputs ([239eb69](https://gitlab.com/homescreen.rocks/payment-system/commit/239eb69)), closes [#56](https://gitlab.com/homescreen.rocks/payment-system/issues/56)


### Features

* **Data:** add seeding script ([dd3808d](https://gitlab.com/homescreen.rocks/payment-system/commit/dd3808d)), closes [#28](https://gitlab.com/homescreen.rocks/payment-system/issues/28)



## [1.0.0-beta.3](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-beta.2...1.0.0-beta.3) (2019-06-19)


### Bug Fixes

* **Transactions:** do not send product images in TransactionResponse ([c52a989](https://gitlab.com/homescreen.rocks/payment-system/commit/c52a989)), closes [#55](https://gitlab.com/homescreen.rocks/payment-system/issues/55)



## [1.0.0-beta.2](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-beta.1...1.0.0-beta.2) (2019-06-19)


### Bug Fixes

* **DockerComposer:** add complete docker-compose.yml file ([59384d4](https://gitlab.com/homescreen.rocks/payment-system/commit/59384d4))
* **Transactions:** allow CashDesk user to list Transactions of all users ([a52d106](https://gitlab.com/homescreen.rocks/payment-system/commit/a52d106))



## [1.0.0-beta.1](https://gitlab.com/homescreen.rocks/payment-system/compare/1.0.0-beta.0...1.0.0-beta.1) (2019-06-18)


### Bug Fixes

* **Mapping:** fixes Automapper to use an unbugged version (6.1.0 instead of 6.1.1) ([27ab3b8](https://gitlab.com/homescreen.rocks/payment-system/commit/27ab3b8)), closes [#54](https://gitlab.com/homescreen.rocks/payment-system/issues/54)



## 1.0.0-beta.0 (2019-06-18)


### Bug Fixes

* **Accounts:** fixes an unhandles Exception when placing a card not associated with any account ([5fd6639](https://gitlab.com/homescreen.rocks/payment-backend/commit/5fd6639))
* **Accounts:** returns full AccountInformationen on both, update and creation ([13315ad](https://gitlab.com/homescreen.rocks/payment-backend/commit/13315ad))
* **API:** working with eager loading ([fcfaac9](https://gitlab.com/homescreen.rocks/payment-backend/commit/fcfaac9))
* **Core:** fix DbContext service lifetime ([9431bdf](https://gitlab.com/homescreen.rocks/payment-backend/commit/9431bdf))
* **Core:** fix error disconnecting NFC client ([6fac6b1](https://gitlab.com/homescreen.rocks/payment-backend/commit/6fac6b1))
* **Core:** optimize code ([6e1eb01](https://gitlab.com/homescreen.rocks/payment-backend/commit/6e1eb01))
* **Fixes:** several small bugs are fixed with this commit ([00ab277](https://gitlab.com/homescreen.rocks/payment-backend/commit/00ab277))
* **HubConnection:** do not allow to join a PoS if this is not configured ([46db45f](https://gitlab.com/homescreen.rocks/payment-backend/commit/46db45f)), closes [#41](https://gitlab.com/homescreen.rocks/payment-backend/issues/41)
* **HubConnections:** fixes disconnecting of cash-desk-clients ([db3c3ab](https://gitlab.com/homescreen.rocks/payment-backend/commit/db3c3ab))
* **HubConnections:** fixes error reporting on duplicate connections to PoS ([12587bc](https://gitlab.com/homescreen.rocks/payment-backend/commit/12587bc))
* **JwtAuthorizationTokenService:** improves token refreshing handling ([907d8a7](https://gitlab.com/homescreen.rocks/payment-backend/commit/907d8a7))
* **Kestrel:** fixes serving of static files by backend ([846a222](https://gitlab.com/homescreen.rocks/payment-backend/commit/846a222))
* **Kestrel:** serve angular SPA's from within /admin and /cashdesk ([03ca64a](https://gitlab.com/homescreen.rocks/payment-backend/commit/03ca64a))
* **Mapper:** fixes several small mapper issues ([471bd12](https://gitlab.com/homescreen.rocks/payment-backend/commit/471bd12))
* **NewCard:** catches an error with unknown cards ([a7f0c28](https://gitlab.com/homescreen.rocks/payment-backend/commit/a7f0c28))
* **NfcClient:** fix build, use dotnet publish ([d7e1a5b](https://gitlab.com/homescreen.rocks/payment-backend/commit/d7e1a5b))
* **NfcClient:** fix namespace ([8df4d14](https://gitlab.com/homescreen.rocks/payment-backend/commit/8df4d14))
* **NfcClient:** fix no-card startup issue ([b3224ae](https://gitlab.com/homescreen.rocks/payment-backend/commit/b3224ae))
* **NfcClient:** make reader selection configurable ([91c8905](https://gitlab.com/homescreen.rocks/payment-backend/commit/91c8905))
* **NfcClient:** use UID of NFC card ([7e2d644](https://gitlab.com/homescreen.rocks/payment-backend/commit/7e2d644))
* **Pagination:** fixes usage of page size default value ([03bc8de](https://gitlab.com/homescreen.rocks/payment-backend/commit/03bc8de)), closes [#36](https://gitlab.com/homescreen.rocks/payment-backend/issues/36)
* **PaymentHub:** fixes correct handling of aborted clients and frees PoS for new connection ([ebaa72b](https://gitlab.com/homescreen.rocks/payment-backend/commit/ebaa72b))
* **PointOfSaleRepo:** fixes an issue combining NfcClient and CashDeskClient to a single PoS ([d2b56d5](https://gitlab.com/homescreen.rocks/payment-backend/commit/d2b56d5))
* **ProductType:** fixes updating product types ([76b39a6](https://gitlab.com/homescreen.rocks/payment-backend/commit/76b39a6))
* **ProductTypes:** fixes creation/update of productTypes ([62be4d5](https://gitlab.com/homescreen.rocks/payment-backend/commit/62be4d5)), closes [#39](https://gitlab.com/homescreen.rocks/payment-backend/issues/39)
* **Readme:** some useless fix ([39c50e9](https://gitlab.com/homescreen.rocks/payment-backend/commit/39c50e9))
* **SignalR:** fix some signalr service errors ([af88b00](https://gitlab.com/homescreen.rocks/payment-backend/commit/af88b00))
* **Transactions:** refactors CreateTransaction messages ([015d31a](https://gitlab.com/homescreen.rocks/payment-backend/commit/015d31a))
* **Transactions:** return full Product as TransactionEntry ([13e5f17](https://gitlab.com/homescreen.rocks/payment-backend/commit/13e5f17))


### Build System

* **Dependencies:** updated all dependencies ([495f4c6](https://gitlab.com/homescreen.rocks/payment-backend/commit/495f4c6))
* **Docker:** fix ([8f86f03](https://gitlab.com/homescreen.rocks/payment-backend/commit/8f86f03))
* **Migrations:** adds Card migration ([dd06afa](https://gitlab.com/homescreen.rocks/payment-backend/commit/dd06afa))
* **Nfc-Client:** fix ([f277492](https://gitlab.com/homescreen.rocks/payment-backend/commit/f277492))
* **Nfc-Client:** fix ([66d5324](https://gitlab.com/homescreen.rocks/payment-backend/commit/66d5324))


### Features

* **Account:** adds LOCKED state to accounts ([9171bf5](https://gitlab.com/homescreen.rocks/payment-backend/commit/9171bf5)), closes [#16](https://gitlab.com/homescreen.rocks/payment-backend/issues/16)
* **API:** adding database connection ([6e96859](https://gitlab.com/homescreen.rocks/payment-backend/commit/6e96859))
* **API:** adds REST endpoint for creating Accounts ([5c96054](https://gitlab.com/homescreen.rocks/payment-backend/commit/5c96054)), closes [#14](https://gitlab.com/homescreen.rocks/payment-backend/issues/14)
* **API:** basic product type routes ([4513ba1](https://gitlab.com/homescreen.rocks/payment-backend/commit/4513ba1))
* **API:** dotnet new webapi ([1612227](https://gitlab.com/homescreen.rocks/payment-backend/commit/1612227))
* **Authentication:** adds JwtAuthentication services and REST login route ([8d26d9b](https://gitlab.com/homescreen.rocks/payment-backend/commit/8d26d9b)), closes [#17](https://gitlab.com/homescreen.rocks/payment-backend/issues/17)
* **Authentication:** adds password check ([83594a1](https://gitlab.com/homescreen.rocks/payment-backend/commit/83594a1)), closes [#33](https://gitlab.com/homescreen.rocks/payment-backend/issues/33)
* **Authorization:** adds account roles to backend ([e9b2d24](https://gitlab.com/homescreen.rocks/payment-backend/commit/e9b2d24))
* **Charge:** adds charge implementation ([46aac99](https://gitlab.com/homescreen.rocks/payment-backend/commit/46aac99))
* **Core:** add entities and models ([e8c3a32](https://gitlab.com/homescreen.rocks/payment-backend/commit/e8c3a32))
* **DB:** first try to use foreingnt keys in entityFramework ([adb2158](https://gitlab.com/homescreen.rocks/payment-backend/commit/adb2158))
* **Debug:** adds a simple nfc-service-emulator to solution ([7fa6147](https://gitlab.com/homescreen.rocks/payment-backend/commit/7fa6147))
* **Emulator:** implement token authentication ([e596912](https://gitlab.com/homescreen.rocks/payment-backend/commit/e596912)), closes [#22](https://gitlab.com/homescreen.rocks/payment-backend/issues/22)
* **EntityFramework:** try and try and try and .. ([d3d8640](https://gitlab.com/homescreen.rocks/payment-backend/commit/d3d8640))
* **Hub:** CashDesk: send current card after joinen a PoS with existing NfcClient ([8761af9](https://gitlab.com/homescreen.rocks/payment-backend/commit/8761af9))
* **JoinPos:** CashDesk can now join and will ne anounced to nfc (if present) ([9eb6ac9](https://gitlab.com/homescreen.rocks/payment-backend/commit/9eb6ac9))
* **Logger:** adds ILogger instead of Console.WriteLine ([48a2fa3](https://gitlab.com/homescreen.rocks/payment-backend/commit/48a2fa3)), closes [#11](https://gitlab.com/homescreen.rocks/payment-backend/issues/11)
* **MSC:** adds put card to reader use-case ([f333739](https://gitlab.com/homescreen.rocks/payment-backend/commit/f333739))
* **MSC:** adds RemovedCard message handling ([adba18c](https://gitlab.com/homescreen.rocks/payment-backend/commit/adba18c))
* **PaymentHub:** adds hello message to /payment-hub ([a89d253](https://gitlab.com/homescreen.rocks/payment-backend/commit/a89d253))
* **PoS:** adds nfc and cashDesk satates to REST model ([c8635af](https://gitlab.com/homescreen.rocks/payment-backend/commit/c8635af)), closes [#37](https://gitlab.com/homescreen.rocks/payment-backend/issues/37)
* **PoS:** fully refactored PoS handling ([1b5e07c](https://gitlab.com/homescreen.rocks/payment-backend/commit/1b5e07c)), closes [#23](https://gitlab.com/homescreen.rocks/payment-backend/issues/23)
* **Product:** adds product image ([642c962](https://gitlab.com/homescreen.rocks/payment-backend/commit/642c962)), closes [#40](https://gitlab.com/homescreen.rocks/payment-backend/issues/40)
* **Purchase:** adds purchase implementation ([9c5c897](https://gitlab.com/homescreen.rocks/payment-backend/commit/9c5c897)), closes [#12](https://gitlab.com/homescreen.rocks/payment-backend/issues/12)
* **REST:** add create/update for product ([60f4c72](https://gitlab.com/homescreen.rocks/payment-backend/commit/60f4c72))
* **REST:** add product type routes ([b625299](https://gitlab.com/homescreen.rocks/payment-backend/commit/b625299)), closes [#26](https://gitlab.com/homescreen.rocks/payment-backend/issues/26)
* **REST:** add Products routes ([e5ade62](https://gitlab.com/homescreen.rocks/payment-backend/commit/e5ade62))
* **REST:** adds API for getting accounts /accounts[/{id}] ([07e1021](https://gitlab.com/homescreen.rocks/payment-backend/commit/07e1021)), closes [#15](https://gitlab.com/homescreen.rocks/payment-backend/issues/15)
* **REST:** adds roles property to Accounts roles ([b7d378c](https://gitlab.com/homescreen.rocks/payment-backend/commit/b7d378c))
* **REST:** adds Roles route ([672776e](https://gitlab.com/homescreen.rocks/payment-backend/commit/672776e)), closes [#52](https://gitlab.com/homescreen.rocks/payment-backend/issues/52)
* **REST:** implement filter posibilities for all entities ([986f6c2](https://gitlab.com/homescreen.rocks/payment-backend/commit/986f6c2)), closes [#29](https://gitlab.com/homescreen.rocks/payment-backend/issues/29)
* **Routing:** add GUID contstraints for routes containing IDs ([8cbcba6](https://gitlab.com/homescreen.rocks/payment-backend/commit/8cbcba6))
* **Services:** adding AccountRepository, adding cardId to Account Class ([0eed7f3](https://gitlab.com/homescreen.rocks/payment-backend/commit/0eed7f3))
* **SignalR:** adds connection handling for GeneralSubscribers ([d843763](https://gitlab.com/homescreen.rocks/payment-backend/commit/d843763)), closes [#43](https://gitlab.com/homescreen.rocks/payment-backend/issues/43)
* **SignalR:** send updated NewCard to PoS clients after successful transaction ([624d042](https://gitlab.com/homescreen.rocks/payment-backend/commit/624d042))
* **Transaction:** implementing a comment property for transactions ([eeaf930](https://gitlab.com/homescreen.rocks/payment-backend/commit/eeaf930)), closes [#19](https://gitlab.com/homescreen.rocks/payment-backend/issues/19)
* **TransactionRepo:** implementing functions ([8f93fdb](https://gitlab.com/homescreen.rocks/payment-backend/commit/8f93fdb))
* **Transactions:** add transactions ([e37cb28](https://gitlab.com/homescreen.rocks/payment-backend/commit/e37cb28))
* **Transactions:** adds API endpoint to list all transactions of an user ([3839020](https://gitlab.com/homescreen.rocks/payment-backend/commit/3839020)), closes [#3](https://gitlab.com/homescreen.rocks/payment-backend/issues/3)
* **Validations:** adds validation for unique key fields on creation and updating entities ([d74792a](https://gitlab.com/homescreen.rocks/payment-backend/commit/d74792a)), closes [#34](https://gitlab.com/homescreen.rocks/payment-backend/issues/34)
* **Websocket:** adds registration call for NFC clients ([2563338](https://gitlab.com/homescreen.rocks/payment-backend/commit/2563338))
