namespace nfc_client.Services
{
    public interface IPaymentSignalrService
    {
        void SendCardInformation(string cardId);
        void SendRemovedCardInformation();
    }
}