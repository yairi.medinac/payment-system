using System;

namespace nfc_client.Services
{
    public class TokenEventArgs : EventArgs
    {
        public readonly string Token;

        public TokenEventArgs(string token)
        {
            Token = token;
        }
    }
}