namespace nfc_client.Services
{
    public interface IPcScService
    {
        void StartListening();
        string GetCurrentCardId();
    }
}