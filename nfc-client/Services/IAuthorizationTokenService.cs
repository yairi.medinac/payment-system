using System;

namespace nfc_client.Services
{
    public interface IAuthorizationTokenService
    {
        string CurrentToken();
        void RefreshToken();
        event EventHandler<TokenEventArgs> TokenChanged;
    }
}