using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using nfc_client.Models;

namespace nfc_client.Services
{
    public abstract class AbstractSignalrService
    {
        protected HubConnection Connection;
        protected readonly ClientSettings Config;
        protected readonly ILogger<PaymentSignalrService> Logger;
        protected readonly IServiceProvider Services;
        private readonly IAuthorizationTokenService _authorizationTokenService;
        protected string CurrentJwtToken;

        protected AbstractSignalrService(IOptions<ClientSettings> config, ILogger<PaymentSignalrService> logger,
            IServiceProvider services, IAuthorizationTokenService authorizationTokenService)
        {
            Config = config.Value;
            Logger = logger;
            Services = services;
            _authorizationTokenService = authorizationTokenService;
            _authorizationTokenService.TokenChanged += OnTokenChanged;
        }

        ~AbstractSignalrService()
        {
            _authorizationTokenService.TokenChanged -= OnTokenChanged;
        }

        protected void InitializeService()
        {
            Connection = new HubConnectionBuilder()
                .WithUrl(Config.ServerAddress,
                    options => { options.AccessTokenProvider = () => Task.FromResult(CurrentJwtToken); })
                .Build();
            Connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await Connection.StartAsync();
            };
        }

        private void OnTokenChanged(object sender, TokenEventArgs args)
        {
            Logger.LogDebug("New token received: " + args.Token);
            CurrentJwtToken = args.Token;
        }

        protected void RegisterHandler<T>(MethodTypes methodType, Action<T> handler)
        {
            Logger.LogDebug("registered a message for " + methodType);
            Connection.On(methodType.ToString(), handler);
        }

        protected void RegisterHandler(MethodTypes methodType, Action handler)
        {
            Logger.LogDebug("registered a message for " + methodType);
            Connection.On(methodType.ToString(), handler);
        }
    }
}