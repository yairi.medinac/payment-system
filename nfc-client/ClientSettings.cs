using System;

namespace nfc_client
{
    public class ClientSettings
    {
        public string ServerAddress { get; set; }
        public string LoginAddress { get; set; }
        public Guid PosId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int CardReaderId { get; set; }
    }
}