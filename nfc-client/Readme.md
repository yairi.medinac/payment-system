Installation
=

* make sure NFC reader drivers installed
* Linux: if not already started, start the PCSCD daemon
* make sure the payment-backend is started
* check the configuration of the nfc-client
* start the nfc-client `dotnnet run`