using System;

namespace ClientEmulator
{
    public class ClientSettings
    {
        public Guid PosId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}