﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using ClientEmulator.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ClientEmulator
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            var serviceProvider = serviceCollection.BuildServiceProvider();

            var logger = serviceProvider.GetService<ILogger>();

            IServiceEmulator service;

            Console.WriteLine("NFC Emulator Service");

            Console.WriteLine("Type \"1\" to send join as NFC, \"2\" to join as CashDesk, other to exit");
            var input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    service = serviceProvider.GetRequiredService<NfcService>();
                    Console.WriteLine(
                        "Wait for an authorization token, and type anything to start connecting the SignalR-Hub.");
                    Console.ReadLine();
                    await service.ConnectHub();
                    await service.SendJoin();
                    while (true)
                    {
                        Console.WriteLine("Sending NewCard");
                        await ((NfcService) service).SendNewCard();
                        Thread.Sleep(20000);
//                        Console.WriteLine("Type any to send RemoveCard.");
                        Console.WriteLine("Sending RemoveCard.");
//                        Console.ReadLine();
                        await ((NfcService) service).SendRemovedCard();
                        Thread.Sleep(5000);
                    }
                case "2":
                    service = serviceProvider.GetRequiredService<CashDeskService>();
                    Console.WriteLine(
                        "Wait for an authorization token, and type anything to start connecting the SignalR-Hub.");
                    Console.ReadLine();
                    await service.ConnectHub();
                    await service.SendJoin();
                    break;
                default:
                    return;
            }

            Console.WriteLine("Type any to close socket and quit.");
            Console.ReadLine();

            await service.CloseHub();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();

            services
                .AddOptions()
                .Configure<ClientSettings>(config)
                .AddLogging(builder => builder.AddConsole())
                .Configure<LoggerFilterOptions>(options => options.MinLevel = LogLevel.Debug)
                .AddSingleton<NfcService>()
                .AddSingleton<IAuthorizationTokenService, JwtAuthorizationTokenService>()
                .AddSingleton<CashDeskService>();
        }
    }
}