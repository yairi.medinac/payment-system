using System.Threading.Tasks;

namespace ClientEmulator
{
    public interface IServiceEmulator
    {
        Task ConnectHub();
        Task SendJoin();
        Task CloseHub();
    }
}