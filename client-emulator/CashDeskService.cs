using System;
using System.Threading.Tasks;
using ClientEmulator.Services;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PaymentBackend.Entities;
using PaymentBackend.Models;

namespace ClientEmulator
{
    public class CashDeskService : IServiceEmulator
    {
        private readonly HubConnection _connection;
        private readonly ClientSettings _config;
        private readonly ILogger<CashDeskService> _logger;
        private readonly IAuthorizationTokenService _authorizationTokenService;
        private string _currentJwtToken;

        public CashDeskService(IOptions<ClientSettings> config, ILogger<CashDeskService> logger,
            IAuthorizationTokenService authorizationTokenService)
        {
            _logger = logger;
            _config = config.Value;
            _authorizationTokenService = authorizationTokenService;
            _authorizationTokenService.TokenChanged += OnTokenChanged;
            _connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/payment-hub",
                    options => { options.AccessTokenProvider = () => Task.FromResult(_currentJwtToken); })
                .Build();
        }

        ~CashDeskService()
        {
            _authorizationTokenService.TokenChanged -= OnTokenChanged;
        }

        private void OnTokenChanged(object sender, TokenEventArgs args)
        {
            _logger.LogDebug("New token received: " + args.Token);
            _currentJwtToken = args.Token;
        }


        public Task ConnectHub()
        {
            _connection.On<string>("Error", (message) =>
            {
                Console.WriteLine("Received: Error!!!");
                Console.WriteLine(message);
            });
            _connection.On<string>("JoinRequired", (message) =>
            {
                Console.WriteLine("Received: JoinRequired");
                Console.WriteLine(message);
            });
            _connection.On<PointOfSale>("Welcome", (message) =>
            {
                Console.WriteLine("Received: Welcome");
                Console.WriteLine(message);
            });
            _connection.On<PointOfSale>("NewCardReader", (message) =>
            {
                Console.WriteLine("Received: NewCardReader");
                Console.WriteLine(message);
            });
            _connection.On<PointOfSaleDevice>("DeviceLeftPos", (message) =>
            {
                Console.WriteLine("Received: DeviceLeftPos");
                Console.WriteLine(message);
            });
            _connection.On<CardInfo>("NewCard", (message) =>
            {
                Console.WriteLine("Received: NewCard");
                Console.WriteLine(message);
            });
            _connection.On<CardInfo>("RemovedCard", _ => { Console.WriteLine("Received: RemovedCard"); });


            return _connection.StartAsync();
        }

        public Task SendJoin()
        {
            var joinMessage = new PointOfSaleDevice
            {
                PosId = _config.PosId,
                Type = PosClientType.CashDesk
            };

            _logger.LogDebug(joinMessage.ToString());
            return _connection.SendAsync("JoinPointOfSale", joinMessage);
        }

        public Task CloseHub()
        {
            return _connection.StopAsync();
        }
    }
}