using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Timers;
using Microsoft.Extensions.Options;
using PaymentBackend.Models;

namespace ClientEmulator.Services
{
    class JwtAuthorizationTokenService : IAuthorizationTokenService
    {
        private string _currentToken;
        private readonly HttpClient _httpClient = new HttpClient();
        private EventHandler<TokenEventArgs> _tokenChanged;
        private readonly ClientSettings _config;
        private readonly Timer _tokenValidationTimer = new Timer();

        public JwtAuthorizationTokenService(IOptions<ClientSettings> config)
        {
            _config = config.Value;
            _tokenValidationTimer.Elapsed += (sender, e) => RefreshToken();
            RefreshToken();
        }

        public string CurrentToken()
        {
            return _currentToken;
        }

        public void RefreshToken()
        {
            _tokenValidationTimer.Stop();

            var token = LoadToken();
            if (token == null) return;

            _tokenValidationTimer.AutoReset = false;
            _tokenValidationTimer.Interval = token.ValidTo.Subtract(DateTime.UtcNow).TotalMilliseconds - 10000;

            _tokenValidationTimer.Start();
        }

        private JwtSecurityToken LoadToken()
        {
            var loginRequest = new LoginRequest
            {
                Username = _config.Username,
                Password = _config.Password
            };
            var uri = new Uri("https://localhost:5001/api/v1/login");
            var response = _httpClient.PostAsJsonAsync(uri, loginRequest)
                .ContinueWith(r => r.Result.Content.ReadAsAsync<LoginResponse>().Result)
                .Result;
            _currentToken = response?.Token;
            _tokenChanged?.Invoke(this, new TokenEventArgs(_currentToken));

            if (response?.Token == null) return null;

            var handler = new JwtSecurityTokenHandler();
            return handler.ReadToken(response.Token) as JwtSecurityToken;
        }

        public event EventHandler<TokenEventArgs> TokenChanged
        {
            add
            {
                value.Invoke(this, new TokenEventArgs(_currentToken));
                _tokenChanged += value;
            }
            remove { _tokenChanged -= value; }
        }
    }
}