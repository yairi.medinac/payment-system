using System;

namespace ClientEmulator.Services
{
    public interface IAuthorizationTokenService
    {
        string CurrentToken();
        void RefreshToken();
        event EventHandler<TokenEventArgs> TokenChanged;
    }
}