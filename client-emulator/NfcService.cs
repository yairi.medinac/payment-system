using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ClientEmulator.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PaymentBackend.Entities;
using PaymentBackend.Models;

namespace ClientEmulator
{
    public class NfcService : IServiceEmulator
    {
        private readonly HubConnection _connection;
        private readonly ClientSettings _config;
        private readonly ILogger<NfcService> _logger;
        private readonly IAuthorizationTokenService _authorizationTokenService;
        private string _currentJwtToken;

        private int _i = 0;

        public NfcService(IOptions<ClientSettings> config, ILogger<NfcService> logger,
            IAuthorizationTokenService authorizationTokenService)
        {
            _logger = logger;
            _config = config.Value;
            _authorizationTokenService = authorizationTokenService;
            _authorizationTokenService.TokenChanged += OnTokenChanged;
            _connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/payment-hub",
                    options => { options.AccessTokenProvider = () => Task.FromResult(_currentJwtToken); })
                .Build();
        }

        ~NfcService()
        {
            _authorizationTokenService.TokenChanged -= OnTokenChanged;
        }

        private void OnTokenChanged(object sender, TokenEventArgs args)
        {
            _logger.LogDebug("New token received: " + args.Token);
            _currentJwtToken = args.Token;
        }

        public Task ConnectHub()
        {
            _connection.On<ProblemDetails>("Error", (message) =>
            {
                Console.WriteLine("Received: Error");
                Console.WriteLine($"{message.Type}: {message.Detail}");
            });
            _connection.On<string>("JoinRequired", (message) =>
            {
                Console.WriteLine("Received: JoinRequired");
                Console.WriteLine(message);
            });
            _connection.On<PointOfSale>("Welcome", (message) =>
            {
                Console.WriteLine("Received: Welcome");
                Console.WriteLine(message);
            });
            _connection.On<PointOfSale>("NewCashDesk", (message) =>
            {
                Console.WriteLine("Received: NewCashDesk");
                Console.WriteLine(message);
            });
            _connection.On<PointOfSaleDevice>("DeviceLeftPos", (message) =>
            {
                Console.WriteLine("Received: DeviceLeftPos");
                Console.WriteLine(message);
            });
            _connection.On<CardInfo>("NewCard", (message) =>
            {
                Console.WriteLine("Received: NewCard");
                Console.WriteLine(message);
            });
            _connection.On("GetCard", () =>
            {
                Console.WriteLine("Received: GetCard");
                SendCurrentCard("123456789");
                Console.WriteLine("Send CurrentCard: 123456789");
            });

            return _connection.StartAsync();
        }

        public Task SendJoin()
        {
            var joinMessage = new PointOfSaleDevice
            {
                PosId = _config.PosId,
                Type = PosClientType.NfcReader
            };

            return _connection.SendAsync("JoinPointOfSale", joinMessage);
        }

        public Task SendNewCard()
        {
            var cards = new List<Card>
            {
                new Card {Id = "1234"},
                new Card {Id = "12345"},
                new Card {Id = "123456"},
                new Card {Id = "1234567"},
                new Card {Id = "123456789"}
            };

            if (_i > cards.Capacity)
            {
                _i = 0;
            }


            return _connection.SendAsync("NewCard", cards[_i++ % cards.Count]);
        }

        public Task SendRemovedCard()
        {
            return _connection.SendAsync("RemovedCard");
        }

        public Task SendCurrentCard(string cardId)
        {
            var card = new Card
            {
                Id = cardId
            };
            return _connection.SendAsync("CurrentCard", card);
        }

        public Task SendGetCard()
        {
            return _connection.SendAsync("GetCard");
        }

        public Task CloseHub()
        {
            return _connection.StopAsync();
        }
    }
}