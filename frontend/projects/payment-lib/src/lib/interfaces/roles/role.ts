import {uuid} from '../../types/uuid';

export interface Role {
  id: uuid;
  name: string;
  discountPercent: number;
  active: boolean;
}
