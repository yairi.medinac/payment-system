import {SubscriptionNotificationBase} from './subscription-notification-base';
import {PointOfSaleInfo} from '../point-of-sales/point-of-sale-info';
import {SubscriptionEventClasses} from './subscription-event-classes';

export class SubscriptionNotificationPointOfSaleInfo extends SubscriptionNotificationBase {
  eventClass = SubscriptionEventClasses.PointOfSaleInfo;
  content: PointOfSaleInfo;
}
