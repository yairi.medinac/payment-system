import {SubscriptionEventTypes} from './subscription-event-types';

export abstract class SubscriptionNotificationBase {
  public eventType: SubscriptionEventTypes;
}
