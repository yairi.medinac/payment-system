import {SubscriptionNotificationPointOfSaleInfo} from './subscription-notification-point-of-sale-info';

export type SubscriptionNotification =
  SubscriptionNotificationPointOfSaleInfo;
