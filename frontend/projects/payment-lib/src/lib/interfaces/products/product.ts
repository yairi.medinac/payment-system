import {ProductType} from '../product-types/product-type';
import {uuid} from '../../types/uuid';

export interface Product {
  id: uuid;
  name: string;
  price: number;
  defaultPrice: number;
  description: string;
  image: string;
  order: number;
  locked: boolean;
  productType: ProductType;
  priceFixed: boolean;
}
