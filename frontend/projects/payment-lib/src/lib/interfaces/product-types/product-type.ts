import {uuid} from '../../types/uuid';

export interface ProductType {
  id: uuid;
  name: string;
  order: number;
  locked: boolean;
  image: string;
}
