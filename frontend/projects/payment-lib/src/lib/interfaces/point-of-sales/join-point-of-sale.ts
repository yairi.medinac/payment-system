import {uuid} from '../../types/uuid';
import {PosClientType} from './pos-client-type';

export class JoinPointOfSale {
  type: PosClientType;
  posId: uuid;
}
