import {Role} from '../roles/role';
import {uuid} from '../../types/uuid';

export interface AccountInfo {
  id: uuid;
  username: string;
  cardId: string;
  balance: number;
  overdraft: number;
  locked: boolean;
  roles: Role[];
  password?: string;
}
