import {Role} from '../roles/role';

export interface UpdateAccount {
  username: string;
  cardId: string;
  locked: boolean;
  overdraft: number;
  roles: Array<Role>;
}
