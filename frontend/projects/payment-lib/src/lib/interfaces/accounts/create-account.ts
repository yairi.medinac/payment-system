import {Role} from '../roles/role';

export interface CreateAccount {
  username: string;
  password: string;
  cardId: string;
  overdraft: number;
  roles: Array<Role>;
}
