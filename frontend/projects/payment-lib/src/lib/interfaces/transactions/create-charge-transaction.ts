import {uuid} from '../../types/uuid';

export interface CreateChargeTransaction {
  amount: number;
  accountId: uuid;
  pointOfSaleId: uuid;
  comment: string;
}

