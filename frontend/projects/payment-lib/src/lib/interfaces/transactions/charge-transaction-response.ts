import {AccountInfo} from '../accounts/account-info';
import {PointOfSaleInfo} from '../point-of-sales/point-of-sale-info';
import {Product} from '../products/product';
import {uuid} from '../../types/uuid';

export interface ChargeTransactionResponse {
  id: uuid;
  account: AccountInfo;
  pointOfSale: PointOfSaleInfo;
  sum: number;
  timestamp: string;
  cancelled: string | null;
  comment: string;
  entriesGrouped?: { product: Product, price: number, amount: number }[];
  transactionType: 'ChargeTransaction';
}
