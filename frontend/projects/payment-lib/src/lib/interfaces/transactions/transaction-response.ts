import {ChargeTransactionResponse} from './charge-transaction-response';
import {PurchaseTransactionResponse} from './purchase-transaction-response';

export type TransactionResponse = ChargeTransactionResponse | PurchaseTransactionResponse;
