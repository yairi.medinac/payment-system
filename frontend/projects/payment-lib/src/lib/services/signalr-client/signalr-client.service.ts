import {Injectable} from '@angular/core';
import * as signalR from '@aspnet/signalr';
import {HubConnection} from '@aspnet/signalr';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {JwtTokenService} from '../jwt-token/jwt-token.service';
import {map} from 'rxjs/operators';
import {JoinPointOfSale} from '../../interfaces/point-of-sales/join-point-of-sale';
import {uuid} from '../../types/uuid';
import {AuthService} from '../auth/auth.service';
import {CardInfo} from '../../interfaces/cards/card-info';
import {PosClientType} from '../../interfaces/point-of-sales/pos-client-type';
import {PointOfSaleInfo} from '../../interfaces/point-of-sales/point-of-sale-info';

@Injectable({
  providedIn: 'root'
})
export class SignalrClientService {
  connectionChanged = new Subject<boolean>();

  private connection: HubConnection;
  private currentCardSubject: BehaviorSubject<CardInfo | null> = new BehaviorSubject<CardInfo | null>(null);
  // private nfcClientError: ToastMessage = {
  //   header: 'Card reader loginError',
  //   autoHide: null,
  //   body: 'There is no NFC Card Reader connected to this Point of Sale!',
  //   classname: 'bg-warning text-dark'
  // };

  constructor(
    private http: HttpClient,
    private jwtService: JwtTokenService,
    private authService: AuthService
  ) {
    jwtService.currentToken$.subscribe(async t => {
      if (t === null) {
        await this.connection.stop();
      } else {
        this.startConnection(t);
      }
    });
  }

  get currentCard(): Observable<CardInfo | null> {
    return this.currentCardSubject.asObservable();
  }

  get currentDiscount(): Observable<number> {
    return this.currentCardSubject.asObservable().pipe(
      map(cc => {
        if (cc !== null && cc.account !== null) {
          return Math.max.apply(Math, cc.account.roles.map(role => role.discountPercent));
        } else {
          return 0;
        }
      })
    );
  }

  public joinPos(pos: uuid, posClientType: PosClientType) {
    this.connection.invoke('JoinPointOfSale', {
      type: posClientType,
      posId: pos
    }).catch(err => console.error(err));
  }

  private startConnection(token: string) {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl('/payment-hub?access_token=' + token)
      .build();

    this.connection.start().then().catch(err => {
      this.connectionChanged.next(false);
    });

    this.connection.onclose(err => {
      this.connectionChanged.next(false);
    });

    this.connection.on('JoinRequired', () => {
      this.joinPos(this.authService.pointOfSaleId, PosClientType.CashDesk);
    });

    this.connection.on('NewCard', (ev: CardInfo) => {
      this.currentCardSubject.next(ev);
    });

    this.connection.on('RemovedCard', () => {
      this.currentCardSubject.next(null);
    });

    this.connection.on('Welcome', (newPosInfo: PointOfSaleInfo) => {
      this.currentCardSubject.next(null);
      this.connectionChanged.next(true);
      // if (newPosInfo.hasNfcClient === false) {
      //   this.toastService.show(this.nfcClientError);
      // } else {
      //   this.toastService.remove(this.nfcClientError);
      // }
    });

    this.connection.on('NewCardReader', (newPosInfo: PointOfSaleInfo) => {
      this.currentCardSubject.next(null);
      if (newPosInfo.hasNfcClient === true) {
        // this.toastService.remove(this.nfcClientError);
      }
    });

    this.connection.on('DeviceLeftPos', (deviceLeftMessage) => {
      this.currentCardSubject.next(null);
      if (deviceLeftMessage.type === 'NfcReader') {
        // this.toastService.show(this.nfcClientError);
      }
    });
  }
}
