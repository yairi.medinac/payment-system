import {Injectable} from '@angular/core';
import {AES, enc} from 'crypto-ts';
import {BehaviorSubject, Observable} from 'rxjs';
import {JwtTokenService} from '../jwt-token/jwt-token.service';
import {LoginResponse} from '../../interfaces/login/login-response';
import {uuid} from '../../types/uuid';
import {LoginRequest} from '../../interfaces/login/login-request';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public static SALT = 'fn2q_70f84$§';
  private currentPin: string = null;
  // tslint:disable-next-line:variable-name
  private _isLoggedIn = false;

  private currentLoginStateSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this._isLoggedIn);

  constructor(private jwtTokenService: JwtTokenService) {
  }

  get isLoggedIn$(): Observable<boolean> {
    return this.currentLoginStateSubject.asObservable();
  }

  lock(): void {
    this.currentPin = null;
    this._isLoggedIn = false;
    this.jwtTokenService.clearToken();
    this.currentLoginStateSubject.next(this._isLoggedIn);
  }

  unlock(pin: string): Observable<LoginResponse> {
    this.currentPin = pin;
    this._isLoggedIn = true;
    return this.login();
  }

  isLocked(): boolean {
    return !this._isLoggedIn;
  }

  private get pin(): string {
    return this.currentPin;
  }

  get pointOfSaleId(): string | null {
    return localStorage.getItem('posId');
  }

  set pointOfSaleId(posId: uuid) {
    localStorage.setItem('posId', posId);
  }

  get username(): string | null {
    return localStorage.getItem('user');
  }


  set username(user: string) {
    localStorage.setItem('user', user);
  }

  get password(): string | null {
    const key = localStorage.getItem('key');
    if (!key || this.currentPin === null) {
      return null;
    }
    const saltedPin = [this.pin, AuthService.SALT].join('');
    const bytes = AES.decrypt(key, saltedPin);
    return bytes.toString(enc.Utf8);
  }

  setPassword(password: string, pin: string) {
    const saltedPin = [pin, AuthService.SALT].join('');
    localStorage.setItem('key', AES.encrypt(password, saltedPin).toString());
  }

  private login() {
    const loginRequest: LoginRequest = {
      username: this.username,
      password: this.password
    };
    return this.jwtTokenService.refreshToken(loginRequest);
  }
}
