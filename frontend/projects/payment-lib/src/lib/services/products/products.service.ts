import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {concatMap, expand, map, toArray} from 'rxjs/operators';
import {ProductFilter} from './product-filter';
import {Product} from '../../interfaces/products/product';
import {PagedResult} from '../paged-result';
import {UpdateProduct} from '../../interfaces/products/update-product';
import {CreateProduct} from '../../interfaces/products/create-product';
import {AbstractApiService} from '../abstract-api.service';
import {Filter} from '../../filter/filter';
import {SortDirection} from '../../filter/sort-direction';
import {RelationalOperator} from '../../filter/relational-operator';
import {uuid} from '../../types/uuid';

@Injectable({
  providedIn: 'root'
})
export class ProductsService extends AbstractApiService<Product> {
  baseUrl = '/api/v1/Products';

  constructor(public http: HttpClient) {
    super();
  }

  getFilteredProducts(filter: Filter<ProductFilter>): Observable<PagedResult<Product>> {
    // todo: move this to caller
    filter.setSort('order', SortDirection.ASC);
    filter.setSort('name', SortDirection.ASC);
    filter.setFilter('locked', RelationalOperator.EQUALS, false);
    filter.setFilter('productType.Locked', RelationalOperator.EQUALS, false);
    return this.getFiltered(this.baseUrl, filter);
  }

  public fetch = (filter: Filter<ProductFilter>) => this.getFiltered(this.baseUrl, filter);

  getProductById(id: uuid): Observable<Product | null> {
    const filter = new Filter(ProductFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('Product with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift())
    );
  }

  createProduct(createMessage: CreateProduct): Observable<Product> {
    return this.postGeneric<CreateProduct, Product>(this.baseUrl, createMessage);
  }

  updateProduct(productId: uuid, updateMessage: UpdateProduct): Observable<Product> {
    return this.putGeneric<UpdateProduct, Product>(this.baseUrl + '/' + productId, updateMessage);
  }

  delete(product: Product) {
    return this.deleteGeneric<null>(this.baseUrl + '/' + product.id);
  }

  getAllProductsFiltered(filter: Filter<ProductFilter>): Observable<Product[]> {
    filter.setPage(1);
    return this.getFilteredProducts(filter).pipe(
      expand(page => {
        if (page.currentPage < page.pageCount) {
          filter.setPage(page.currentPage + 1);
          return this.getFilteredProducts(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      map(p => {
        p.defaultPrice = p.price;
        return p;
      }),
      toArray()
    );
  }
}
