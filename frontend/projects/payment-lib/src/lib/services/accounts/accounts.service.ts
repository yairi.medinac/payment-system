import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AccountFilter} from './account-filter';
import {UpdateAccount} from '../../interfaces/accounts/update-account';
import {CreateAccount} from '../../interfaces/accounts/create-account';
import {UpdateAccountPassword} from '../../interfaces/accounts/update-account-password';
import {Filter} from '../../filter/filter';
import {PagedResult} from '../paged-result';
import {AbstractApiService} from '../abstract-api.service';
import {uuid} from '../../types/uuid';
import {AccountInfo} from '../../interfaces/accounts/account-info';
import {RelationalOperator} from '../../filter/relational-operator';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AccountsService extends AbstractApiService<AccountInfo> {
  baseUrl = '/api/v1/Accounts';

  constructor(public http: HttpClient) {
    super();
  }

  save(accountInfo: AccountInfo): Observable<AccountInfo> {
    let $acc: Observable<AccountInfo>;
    if (accountInfo.id) {
      const account: UpdateAccount = {
        username: accountInfo.username,
        cardId: accountInfo.cardId,
        locked: accountInfo.locked,
        overdraft: accountInfo.overdraft,
        roles: accountInfo.roles.filter(r => r !== null)
      };
      $acc = this.updateAccount(accountInfo.id, account);
    } else {
      const account: CreateAccount = {
        username: accountInfo.username,
        password: accountInfo.password,
        cardId: accountInfo.cardId,
        overdraft: accountInfo.overdraft,
        roles: accountInfo.roles.filter(r => r !== null)
      };
      $acc = this.createAccount(account);
    }
    return $acc;
  }

  getAccountInfoById(id: uuid): Observable<AccountInfo | null> {
    const filter = new Filter(AccountFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('AccountInfo with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift())
    );
  }

  // +----------------------------------------------------------------------------+
  // |                    START Backend REST APIs                                 |
  // +----------------------------------------------------------------------------+

  public fetch = (filter: Filter<AccountFilter>) => this.getFilteredAccounts(filter);

  getFilteredAccounts(filter: Filter<AccountFilter>): Observable<PagedResult<AccountInfo>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  createAccount(createMessage: CreateAccount): Observable<AccountInfo> {
    return this.postGeneric<CreateAccount, AccountInfo>(this.baseUrl, createMessage);
  }

  updateAccount(accountId: uuid, updateMessage: UpdateAccount): Observable<AccountInfo> {
    return this.putGeneric<UpdateAccount, AccountInfo>(this.baseUrl + '/' + accountId, updateMessage);
  }

  deleteAccount(accountId: uuid): Observable<null> {
    return this.deleteGeneric<null>(this.baseUrl + '/' + accountId);
  }

  setPassword(accountInfo: AccountInfo): Observable<AccountInfo> {
    const updateAccountPasswordMsg: UpdateAccountPassword = {
      password: accountInfo.password
    };
    return this.patchGeneric<UpdateAccountPassword, AccountInfo>(
      this.baseUrl + '/' + accountInfo.id + '/password',
      updateAccountPasswordMsg
    );
  }

  lockAccount(accountId: uuid): Observable<null> {
    return this.patchGeneric(this.baseUrl + '/' + accountId + '/lock');
  }

  unlockAccount(accountId: uuid): Observable<null> {
    return this.patchGeneric(this.baseUrl + '/' + accountId + '/unlock');
  }

  // +----------------------------------------------------------------------------+
  // |                    END Backend REST APIs                                 |
  // +----------------------------------------------------------------------------+
}
