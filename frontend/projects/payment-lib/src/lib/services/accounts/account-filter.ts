import {AbstractFilter} from '../../filter/abstract-filter';
import {RelationalOperator} from '../../filter/relational-operator';
import {uuid} from '../../types/uuid';

export class AccountFilter extends AbstractFilter {
  public id: Map<RelationalOperator, uuid | uuid[]> = new Map<RelationalOperator, uuid | uuid[]>();
  public username: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public cardId: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public balance: Map<RelationalOperator, number> = new Map<RelationalOperator, number>();
  public overdraft: Map<RelationalOperator, number> = new Map<RelationalOperator, number>();
  public locked: Map<RelationalOperator, boolean> = new Map<RelationalOperator, boolean>();
}
