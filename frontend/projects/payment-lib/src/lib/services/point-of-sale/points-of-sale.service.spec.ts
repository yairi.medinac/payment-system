import { TestBed } from '@angular/core/testing';

import { PointsOfSaleService } from './points-of-sale.service';

describe('PointsOfSaleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PointsOfSaleService = TestBed.get(PointsOfSaleService);
    expect(service).toBeTruthy();
  });
});
