import {AbstractFilter} from '../../filter/abstract-filter';
import {RelationalOperator} from '../../filter/relational-operator';
import {uuid} from '../../types/uuid';

export class PointOfSaleFilter extends AbstractFilter {
  public id: Map<RelationalOperator, uuid | uuid[]> = new Map<RelationalOperator, uuid | uuid[]>();
  public name: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
}
