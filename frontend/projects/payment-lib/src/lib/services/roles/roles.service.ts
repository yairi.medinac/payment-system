import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {concatMap, expand, map, toArray} from 'rxjs/operators';
import {Role} from '../../interfaces/roles/role';
import {PagedResult} from '../paged-result';
import {AbstractApiService} from '../abstract-api.service';
import {Filter} from '../../filter/filter';
import {TransactionsFilter} from '../transactions/transactions-filter';
import {RelationalOperator} from '../../filter/relational-operator';
import {RoleFilter} from './role-filter';
import {uuid} from '../../types/uuid';

@Injectable({
  providedIn: 'root'
})
export class RolesService extends AbstractApiService<Role> {
  baseUrl = '/api/v1/Roles';

  constructor(public http: HttpClient) {
    super();
  }

  getFilteredRoles(filter: Filter<RoleFilter>): Observable<PagedResult<Role>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public fetch = (filter: Filter<RoleFilter>) => this.getFilteredRoles(filter);

  getRoleById(id: uuid): Observable<Role | null> {
    const filter = new Filter(TransactionsFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('Role with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift())
    );
  }

  getAllRolesFiltered(filter: Filter<RoleFilter>): Observable<Role[]> {
    filter.setPage(1);
    return this.getFilteredRoles(filter).pipe(
      expand(page => {
        if (page.currentPage < page.pageCount) {
          filter.setPage(page.currentPage + 1);
          return this.getFilteredRoles(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray()
    );
  }
}
