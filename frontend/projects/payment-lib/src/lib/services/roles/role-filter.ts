import {AbstractFilter} from '../../filter/abstract-filter';
import {RelationalOperator} from '../../filter/relational-operator';

export class RoleFilter extends AbstractFilter {
  public id: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public name: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public discountPercent: Map<RelationalOperator, number | number[]> = new Map<RelationalOperator, number | number[]>();
}
