import {Injectable} from '@angular/core';
import {LoginRequest, LoginResponse} from 'payment-lib';
import {HttpClient} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {BehaviorSubject, Observable} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class JwtTokenService {
  private static loginUrl = '/api/v1/login';
  // tslint:disable-next-line:variable-name
  private _token: string;
  private timerHandle;

  private currentTokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  private account: LoginRequest;

  constructor(private http: HttpClient) {
    // this.config.lockState.subscribe(ls => {
    //   if (ls === true && this.timerHandle !== null) {
    //     this.updateToken(null);
    //   }
    // });
  }

  get currentToken$(): Observable<string> {
    return this.currentTokenSubject.asObservable();
  }

  public refreshToken(loginRequest: LoginRequest): Observable<LoginResponse> {
    this.account = loginRequest;
    return this.loadToken();
  }

  public clearToken() {
    this.updateToken(null);
  }

  private updateToken(token: string) {
    this._token = token;
    sessionStorage.setItem('jwt-token', this._token);
    this.currentTokenSubject.next(this._token);
    if (this.timerHandle !== null) {
      clearTimeout(this.timerHandle);
    }
    if (token === null) {
      return;
    }

    // start reloading:
    const helper = new JwtHelperService();
    const expirationDate = helper.getTokenExpirationDate(this._token);
    if (expirationDate !== null) {
      // todo: setTimeout is not in angular context!!! this seems to be a bug
      this.timerHandle = setTimeout(() => {
        this.loadToken().subscribe();
      }, (expirationDate.getTime() - (new Date()).getTime() - 10000));
    }
  }

  private loadToken(): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(JwtTokenService.loginUrl, this.account)
      .pipe(
        tap(res => this.updateToken(res.token)),
        catchError(err => {
          console.warn(err);
          throw err;
        })
      );
  }
}
