import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {DEFAULT_CURRENCY_CODE, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatCommonModule, MatRippleModule} from '@angular/material/core';
import {MatSliderModule} from '@angular/material/slider';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTabsModule} from '@angular/material/tabs';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {JwtModule} from '@auth0/angular-jwt';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {far} from '@fortawesome/free-regular-svg-icons';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {NgbModalModule, NgbToastModule} from '@ng-bootstrap/ng-bootstrap';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CartEntryComponent} from './components/cart-entry/cart-entry.component';
import {CartComponent} from './components/cart/cart.component';
import {ChargeButtonComponent} from './components/charge-button/charge-button.component';
import {ChargeSelectorComponent} from './components/charge-selector/charge-selector.component';
import {ChargeSidebarComponent} from './components/charge-sidebar/charge-sidebar.component';
import {ChargeValueComponent} from './components/charge-value/charge-value.component';
import {CheckoutButtonComponent} from './components/checkout-button/checkout-button.component';
import {CheckoutSidebarComponent} from './components/checkout-sidebar/checkout-sidebar.component';
import {CustomerDetailsComponent} from './components/customer-details/customer-details.component';
import {FloatingLinkButtonComponent} from './components/floating-link-button/floating-link-button.component';
import {ProductSelectorComponent} from './components/product-selector/product-selector.component';
import {ProductComponent} from './components/product/product.component';
import {OrderIsCancelablePipe} from './directives/order-is-cancelable.pipe';
import {UseWindowHeightDirective} from './directives/use-window-height.directive';
import {CashdeskscreenComponent} from './structure/cashdeskscreen/cashdeskscreen.component';
import {ChargescreenComponent} from './structure/chargescreen/chargescreen.component';
import {ConfigurationComponent} from './structure/configuration/configuration.component';
import {LockscreenComponent} from './structure/lockscreen/lockscreen.component';
import {StartscreenComponent} from './structure/startscreen/startscreen.component';
import {TransactionscreenComponent} from './structure/transactionscreen/transactionscreen.component';
import {ToastsContainerComponent} from './components/toasts-container/toasts-container.component';
import {AuthService, LibModule} from 'payment-lib';
import {environment} from '../environments/environment';
import {LongPressDirective} from './directives/long-press.directive';
import {ProductTypeCardComponent} from './components/product-type-card/product-type-card.component';

export function tokenGetter() {
  return sessionStorage.getItem('jwt-token');
}

@NgModule({
  declarations: [
    AppComponent,
    LockscreenComponent,
    CashdeskscreenComponent,
    ProductSelectorComponent,
    CheckoutSidebarComponent,
    CustomerDetailsComponent,
    CartComponent,
    ProductComponent,
    CartEntryComponent,
    CheckoutButtonComponent,
    UseWindowHeightDirective,
    ConfigurationComponent,
    ChargescreenComponent,
    ChargeSelectorComponent,
    StartscreenComponent,
    FloatingLinkButtonComponent,
    ChargeSidebarComponent,
    ChargeValueComponent,
    ChargeButtonComponent,
    OrderIsCancelablePipe,
    TransactionscreenComponent,
    ToastsContainerComponent,
    LongPressDirective,
    ProductTypeCardComponent
  ],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatCommonModule,
    MatSliderModule,
    MatTabsModule,
    MatRippleModule,
    MatSnackBarModule,
    NgbModalModule,
    JwtModule.forRoot({
      config: {
        tokenGetter,
        whitelistedDomains: ['localhost', 'localhost:4201'],
        blacklistedRoutes: ['']
      }
    }),
    FontAwesomeModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgbToastModule,
    LibModule
  ],
  providers: [
    // todo: setting DEFAULT_CURRENCY_CODE is deprecated and will be removed in angular 10
    // we can remove this line in angular 10 as the currency will be taken from the current locale then
    // https://angular.io/api/core/DEFAULT_CURRENCY_CODE
    {provide: DEFAULT_CURRENCY_CODE, useValue: environment.defaultCurrency},
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
    library.addIconPacks(far);
  }
}
