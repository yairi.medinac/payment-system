import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeValueComponent } from './charge-value.component';

describe('ChargeValueComponent', () => {
  let component: ChargeValueComponent;
  let fixture: ComponentFixture<ChargeValueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeValueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
