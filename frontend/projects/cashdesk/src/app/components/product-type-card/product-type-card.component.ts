import {Component, Input} from '@angular/core';
import {ProductType} from 'payment-lib';

@Component({
  selector: 'app-product-type-card',
  templateUrl: './product-type-card.component.html',
  styleUrls: ['./product-type-card.component.scss']
})
export class ProductTypeCardComponent {
  @Input() productType: ProductType;
}
