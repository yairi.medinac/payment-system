import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeButtonComponent } from './charge-button.component';

describe('ChargeButtonComponent', () => {
  let component: ChargeButtonComponent;
  let fixture: ComponentFixture<ChargeButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
