import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {CardInfo, SignalrClientService} from 'payment-lib';
import {ChargeService} from '../../services/charge-service/charge.service';

@Component({
  selector: 'app-charge-sidebar',
  templateUrl: './charge-sidebar.component.html',
  styleUrls: ['./charge-sidebar.component.scss']
})
export class ChargeSidebarComponent implements OnInit {

  public currentChargeValue$: Observable<number> = this.chargeService.currentChargeAmount;
  public currentCard$: Observable<CardInfo | null> = this.eventSubscriberService.currentCard;

  constructor(private eventSubscriberService: SignalrClientService, public router: Router, private chargeService: ChargeService) {
  }

  ngOnInit() {
  }

}
