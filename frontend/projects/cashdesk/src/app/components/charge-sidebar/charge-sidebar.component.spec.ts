import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeSidebarComponent } from './charge-sidebar.component';

describe('ChargeSidebarComponent', () => {
  let component: ChargeSidebarComponent;
  let fixture: ComponentFixture<ChargeSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
