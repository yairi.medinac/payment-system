import {Component, OnInit} from '@angular/core';
import {ToastsService} from '../../services/toasts.service';

@Component({
  selector: 'app-toasts-container',
  templateUrl: './toasts-container.component.html',
  styleUrls: ['./toasts-container.component.scss']
})
export class ToastsContainerComponent implements OnInit {

  constructor(public toastService: ToastsService) {
  }

  ngOnInit() {
  }

}
