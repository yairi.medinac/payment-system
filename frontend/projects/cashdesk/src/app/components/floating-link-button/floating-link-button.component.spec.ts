import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FloatingLinkButtonComponent} from './floating-link-button.component';

describe('FloatingButtonComponent', () => {
  let component: FloatingLinkButtonComponent;
  let fixture: ComponentFixture<FloatingLinkButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FloatingLinkButtonComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloatingLinkButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
