import {Component, Input, OnInit} from '@angular/core';
import {Product} from 'payment-lib';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @Input() product: Product;
  @Input() discount = 0;

  constructor() {
  }

  ngOnInit() {
  }
}
