import {animate, keyframes, style, transition, trigger} from '@angular/animations';
import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Product} from 'payment-lib';

@Component({
  selector: 'app-cart-entry',
  templateUrl: './cart-entry.component.html',
  styleUrls: ['./cart-entry.component.scss'],
  animations: [
    trigger('onEnter', [
      transition('* => true', [
        style({textShadow: '0 0 0 #F0B429'}),
        animate('400ms ease-in-out', keyframes([
          style({textShadow: '0 0 12px #F0B429', offset: 0.1}),
          style({textShadow: '0 0 0 #F0B429', offset: 1})
        ]))
      ])
    ])
  ]
})
export class CartEntryComponent implements OnInit, OnDestroy {

  @Input() product: Product;
  @Input() isLastChangedItem = false;

  @Output() productDeleted = new EventEmitter<null>();

  // Event Streams
  private unsubscribe: EventEmitter<never> = new EventEmitter<never>();

  constructor() {
  }

  ngOnInit(): void {
  }

  public deleteItem() {
    this.productDeleted.emit();
  }

  ngOnDestroy() {
    this.unsubscribe.next();
  }
}
