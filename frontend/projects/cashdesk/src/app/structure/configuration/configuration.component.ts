import {Component, OnInit} from '@angular/core';
import {AuthService} from 'payment-lib';
import {Router} from '@angular/router';
import {uuid} from 'payment-lib';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {
  posId: uuid;
  username: string;
  password: string;
  pin: string;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.posId = this.authService.pointOfSaleId;
    this.username = this.authService.username;
  }

  save() {
    this.authService.pointOfSaleId = this.posId;
    this.authService.username = this.username;
    this.authService.setPassword(this.password, this.pin);
    this.router.navigate(['/cashdesk']);
  }
}
