import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargescreenComponent } from './chargescreen.component';

describe('ChargescreenComponent', () => {
  let component: ChargescreenComponent;
  let fixture: ComponentFixture<ChargescreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargescreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargescreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
