import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {interval} from 'rxjs';
import {finalize, take, tap} from 'rxjs/operators';
import {AuthService} from 'payment-lib';
import {HttpErrorResponse} from '@angular/common/http';
import {JwtTokenService} from 'payment-lib';

@Component({
  selector: 'app-lockscreen',
  templateUrl: './lockscreen.component.html',
  styleUrls: ['./lockscreen.component.scss']
})
export class LockscreenComponent implements OnInit {
  pin = '';
  error: false | 'server' | 'client' = false;
  errorPinHighlight = false;

  constructor(private authService: AuthService, private jwtTokenService: JwtTokenService, private router: Router) {
  }

  ngOnInit() {
  }

  add(s: string) {
    this.error = false;
    this.pin = this.pin + s;
  }

  backspace() {
    this.pin = this.pin.substr(0, this.pin.length - 1);
  }

  ok() {
    this.error = false;
    this.authService.unlock(this.pin).subscribe(r => {
      this.router.navigate(['/cashdesk']);
    }, (err: HttpErrorResponse) => {
      if (err.status !== 400) {
        this.error = 'server';
      } else {
        this.onError();
      }
    });
  }

  public onError() {
    this.errorPinHighlight = true;
    const $deleteAnimation = interval(80).pipe(take(this.pin.length), tap(() => this.backspace()),
      finalize(() => {
        this.errorPinHighlight = false;
        this.error = 'client';
      }));
    $deleteAnimation.subscribe();
  }
}
