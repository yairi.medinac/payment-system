import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Filter, ProductType, ProductTypeFilter, ProductTypesService, SignalrClientService} from 'payment-lib';
import {ProductTypeSelectorService} from '../../services/product-type-selector.service';

@Component({
  selector: 'app-cashdeskscreen',
  templateUrl: './cashdeskscreen.component.html',
  styleUrls: ['./cashdeskscreen.component.scss']
})
export class CashdeskscreenComponent implements OnInit, OnDestroy {
  productTypes$: Observable<ProductType[]>;
  currentProductType$: Observable<ProductType>;

  private onDestroy$ = new Subject<void>();

  constructor(
    private router: Router,
    private hub: SignalrClientService,
    private productTypesService: ProductTypesService,
    private productTypeSelectorService: ProductTypeSelectorService
  ) {
  }

  ngOnInit() {
    this.productTypes$ = this.productTypesService.getAllProductTypesFiltered(new Filter(ProductTypeFilter));
    this.currentProductType$ = this.productTypeSelectorService.currentProductType$;

    this.hub.connectionChanged.pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(r => {
      if (r === false) {
        this.router.navigate(['locked']);
      }
    });
  }

  selectProductType(productType: ProductType) {
    this.productTypeSelectorService.jump(productType);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
