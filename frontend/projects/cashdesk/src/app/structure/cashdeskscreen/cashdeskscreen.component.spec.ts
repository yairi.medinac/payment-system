import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashdeskscreenComponent } from './cashdeskscreen.component';

describe('CashdeskscreenComponent', () => {
  let component: CashdeskscreenComponent;
  let fixture: ComponentFixture<CashdeskscreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashdeskscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashdeskscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
