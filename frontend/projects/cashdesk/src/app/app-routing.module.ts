import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LockscreenComponent} from './structure/lockscreen/lockscreen.component';
import {CashdeskscreenComponent} from './structure/cashdeskscreen/cashdeskscreen.component';
import {ConfigurationComponent} from './structure/configuration/configuration.component';
import {IsConfiguredGuard} from './guards/is-configured.guard';
import {IsUnlockedGuard} from './guards/is-unlocked.guard';
import {LockGuard} from './guards/lock.guard';
import {ChargescreenComponent} from './structure/chargescreen/chargescreen.component';
import {StartscreenComponent} from './structure/startscreen/startscreen.component';
import {TransactionscreenComponent} from './structure/transactionscreen/transactionscreen.component';

const routes: Routes = [
  {path: 'locked', component: LockscreenComponent, canActivate: [LockGuard]},
  {path: 'configuration', component: ConfigurationComponent},
  {path: 'start', component: StartscreenComponent, canActivate: [IsUnlockedGuard, IsConfiguredGuard]},
  {path: 'cashdesk', component: CashdeskscreenComponent, canActivate: [IsUnlockedGuard, IsConfiguredGuard]},
  {path: 'charge', component: ChargescreenComponent, canActivate: [IsUnlockedGuard, IsConfiguredGuard]},
  {path: 'transaction', component: TransactionscreenComponent, canActivate: [IsUnlockedGuard, IsConfiguredGuard]},
  {
    path: '',
    redirectTo: '/start',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
