import {Directive, EventEmitter, HostListener, OnDestroy, Output, Renderer2} from '@angular/core';
import {interval, Observable, Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';

@Directive({
  selector: '[appLongPress]'
})
export class LongPressDirective implements OnDestroy {
  private static pressOffset = 300;

  @Output() public press: EventEmitter<number> = new EventEmitter<number>();

  private touchendListenFunc: () => void = null;
  private touchcancelListenFunc: () => void = null;
  private touchStarted: Observable<number> = null;
  private onCancel$ = new Subject();

  constructor(private renderer: Renderer2) {
  }

  @HostListener('touchstart', ['$event'])
  @HostListener('mousedown', ['$event'])
  onStart(event) {
    if (this.touchStarted !== null) {
      this.touchStarted = null;
    }
    this.touchStarted = interval(100);
    this.touchStarted.pipe(
      takeUntil(this.onCancel$)
    ).subscribe(n => {
      if (n >= LongPressDirective.pressOffset / 100) {
        this.onCancel$.next(LongPressDirective.pressOffset);
        this.press.emit();
      }
    });
    this.removePreviousTouchListeners();    // avoid mem leaks
    this.touchendListenFunc = this.renderer.listen(event.target, 'touchend', (e) => {
      this.removePreviousTouchListeners();
      this.onEnd(e);
    });
    this.touchcancelListenFunc = this.renderer.listen(event.target, 'touchcancel', (e) => {
      this.removePreviousTouchListeners();
      this.onEnd(e);
    });
  }

  @HostListener('mouseup', ['$event'])
  // @HostListener('touchend', ['$event'])     // don't use these as they are added dynamically
  // @HostListener('touchcancel', ['$event'])  // don't use these as they are added dynamically
  onEnd(event) {
    this.onCancel$.next();
  }

  ngOnDestroy() {
    this.removePreviousTouchListeners();
  }

  private removePreviousTouchListeners() {
    if (this.touchendListenFunc !== null) {
      this.touchendListenFunc();
    }
    if (this.touchcancelListenFunc !== null) {
      this.touchcancelListenFunc();
    }

    this.touchendListenFunc = null;
    this.touchcancelListenFunc = null;
  }
}
