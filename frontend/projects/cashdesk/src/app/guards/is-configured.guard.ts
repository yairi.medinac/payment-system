import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class IsConfiguredGuard implements CanActivate {

  constructor(private configService: AuthService, private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.configService.pointOfSaleId === null
      || this.configService.username === null
      || (this.configService.password === null && !this.configService.isLocked())
    ) {
      this.router.navigate(['/configuration']);
      return false;
    }
    return true;
  }
}
