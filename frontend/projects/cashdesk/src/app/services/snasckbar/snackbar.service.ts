import {Injectable, TemplateRef} from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({providedIn: 'root'})
export class SnackbarService {

  constructor(private snackBar: MatSnackBar) {
  }

  show(textOrTpl: string | TemplateRef<any>, actionLabel: string, options: MatSnackBarConfig = {}) {
    if (!(textOrTpl instanceof TemplateRef)) {
      this.snackBar.open(textOrTpl, actionLabel, options);
    } else {
      this.snackBar.openFromTemplate(textOrTpl, options);
    }
  }
}
