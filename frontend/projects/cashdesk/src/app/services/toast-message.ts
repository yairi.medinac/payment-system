export interface ToastMessage {
  classname: string;
  header: string;
  autoHide: null | number;
  body: string;
}
