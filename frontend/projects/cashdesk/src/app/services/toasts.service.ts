import {Injectable} from '@angular/core';
import {ToastMessage} from './toast-message';

@Injectable({
  providedIn: 'root'
})
export class ToastsService {

  toasts: ToastMessage[] = [];

  show(toast: ToastMessage) {
    this.toasts.push(toast);
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }
}
