export const environment = {
  production: true,
  defaultCurrency: 'EUR', // The ISO 4217 currency code, such as USD for the US dollar and EUR for the euro.
  pointOfSaleId: 'c0e79723-f36d-4d1f-b54d-ab8af1df7ad8'
};
