import {Component} from '@angular/core';
import {NbIconLibraries} from '@nebular/theme';
import {NbAuthService} from '@nebular/auth';
import {delay} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'payment-admin-ui';
  public isAuthenticated: boolean;

  constructor(private iconLibraries: NbIconLibraries, private nbAuthService: NbAuthService) {
    this.iconLibraries.registerFontPack('solid', {packClass: 'fas', iconClassPrefix: 'fa'});
    this.iconLibraries.setDefaultPack('solid');
    this.nbAuthService.onAuthenticationChange().pipe(delay(50)).subscribe(result => this.isAuthenticated = result);
  }
}
