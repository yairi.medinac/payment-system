import * as HttpStatus from 'http-status-codes';
import {HttpErrorResponse} from '@angular/common/http';
import {FormGroup, ValidationErrors} from '@angular/forms';

export class BackendErrorHandler {

  /***
   * Pushed backend validation errors to form properties
   * @param form FormGroup
   */
  public static pushBackendErrorsToForm = (form: FormGroup) => {
    return (err: HttpErrorResponse) => {
      if (err.status === HttpStatus.BAD_REQUEST) {
        console.log(err.error);
        for (const key in err.error.errors as ValidationErrors) {
          if (err.error.errors.hasOwnProperty(key)) {
            const field = form.get(key);
            if (field === null) {
              console.warn('A form field with the name ' + key + ' does not exist!');
            } else {
              field.setErrors({backendErrors: err.error.errors[key]});
            }
          }
        }
      }
    };
  }

}
