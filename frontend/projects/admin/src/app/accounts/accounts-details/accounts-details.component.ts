import {Component, OnInit} from '@angular/core';
import {FormBuilder, ValidationErrors, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import * as HttpStatus from 'http-status-codes';
import {AccountInfo, AccountsService, Filter, Role, RoleFilter, RolesService} from 'payment-lib';
import {Observable} from 'rxjs';
import {BackendErrorHandler} from '../../helper/backend-error.handler';

@Component({
  selector: 'app-accounts-details',
  templateUrl: './accounts-details.component.html',
  styleUrls: ['./accounts-details.component.scss']
})
export class AccountsDetailsComponent implements OnInit {

  roles: Observable<Role[]>;
  account: AccountInfo | null;
  validationErrors: ValidationErrors;
  passwordForm = this.fb.group({
    id: [null],
    password: [null, Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: AccountsService,
    private rolesService: RolesService
  ) {
  }

  ngOnInit() {
    this.account = this.route.snapshot.data.account;
    this.roles = this.rolesService.getAllRolesFiltered((new Filter(RoleFilter)));
  }

  save(accountInfo: AccountInfo): void {
    this.accountService.save(accountInfo).subscribe(
      (acc) => {
        this.router.navigate(['/accounts']);
      },
      (err) => {
        if (err.status === HttpStatus.BAD_REQUEST) {
          console.log(err.error);
          this.validationErrors = err.error.errors;
        }
      },
    );
  }

  savePassword(): void {
    this.passwordForm.patchValue({id: this.account.id});
    this.accountService.setPassword(this.passwordForm.getRawValue()).subscribe(
      (acc) => {
        this.router.navigate(['/accounts/', acc.id]);
        this.passwordForm.get('password').reset();
      },
      BackendErrorHandler.pushBackendErrorsToForm(this.passwordForm)
    );
  }
}
