import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {debounceTime, map, tap} from 'rxjs/operators';
import {merge, Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {ConfirmDialogService} from '../../layout/confirm-dialog/confirm-dialog.service';
import {AccountFilter, AccountInfo, AccountsService, Filter, PagedDataSource, RelationalOperator, SortDirection} from 'payment-lib';

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss']
})
export class AccountsListComponent implements OnInit, AfterViewInit {
  columnsToDisplay = ['id', 'username', 'cardId', 'roles', 'balance', 'overdraft', 'locked', 'actions'];
  filterRow = ['id-filter', 'username-filter', 'cardId-filter', 'roles-filter', 'balance-filter', 'overdraft-filter',
    'locked-filter', 'actions-filter'];
  dataSource: PagedDataSource<AccountInfo, AccountFilter>;

  allFilters$: Observable<[keyof AccountFilter, RelationalOperator, string | number | boolean]>;

  nameFilterValue = new FormControl('');
  nameFilterValue$: Observable<['username', RelationalOperator, string]>;
  cardIdFilterValue = new FormControl('');
  cardIdFilterValue$: Observable<['cardId', RelationalOperator, string]>;
  balanceFilterValue = new FormControl('');
  balanceFilterValue$: Observable<['balance', RelationalOperator, string]>;
  overdraftFilterValue = new FormControl('');
  overdraftFilterValue$: Observable<['overdraft', RelationalOperator, string]>;
  lockedFilterValue = new FormControl('');
  lockedFilterValue$: Observable<['locked', RelationalOperator, boolean]>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private accountsService: AccountsService, private confirmDialog: ConfirmDialogService) {
  }

  ngOnInit() {
    this.dataSource = new PagedDataSource(this.accountsService.fetch, AccountFilter);
    this.dataSource.filter.setSort('username', SortDirection.ASC);
    this.dataSource.loadData();

    const f = new Filter(AccountFilter);
    const queryString = '?page=45&pageSize=100&sorts=-id,username&filters=id==abcd,isLocked==true|false,username@=qwerty';
    f.importFromQueryString(queryString);
    console.log(f);

  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.nameFilterValue$ = this.nameFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['username', RelationalOperator.CONTAINS_CI, v])
    );
    this.cardIdFilterValue$ = this.cardIdFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['cardId', RelationalOperator.CONTAINS_CI, v])
    );
    this.balanceFilterValue$ = this.balanceFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['balance', RelationalOperator.EQUALS, v])
    );
    this.overdraftFilterValue$ = this.overdraftFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['overdraft', RelationalOperator.EQUALS, v])
    );

    this.lockedFilterValue$ = this.lockedFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['locked', RelationalOperator.EQUALS, Boolean(v)])
    );

    this.allFilters$ = merge(
      this.nameFilterValue$,
      this.cardIdFilterValue$,
      this.balanceFilterValue$,
      this.overdraftFilterValue$,
      this.lockedFilterValue$
    );

    merge(this.sort.sortChange, this.paginator.page, this.allFilters$)
      .pipe(
        tap((change: [keyof AccountFilter, RelationalOperator, string | boolean] | Sort | PageEvent) => {
            this.loadAccountInfosPage(change);
          }
        )
      )
      .subscribe();
  }

  loadAccountInfosPage<K extends keyof AccountFilter>(change: [K, RelationalOperator, string | boolean] | Sort | PageEvent) {
    if (change instanceof Array) {
      const key = change[0] as K;
      const operator = change[1];
      const value = change[2];
      if (value === '') {
        this.dataSource.filter.clearFilter(key);
      } else {
        // @ts-ignore
        this.dataSource.filter.setFilter(key, operator, value);
      }
    }
    this.dataSource.filter.clearSort();
    // @ts-ignore
    this.dataSource.filter.setSort(this.sort.active, this.sort._direction);
    this.dataSource.filter.setPage(this.paginator.pageIndex + 1);
    this.dataSource.filter.setPageSize(this.paginator.pageSize);
    this.dataSource.loadData();
  }

  delete(acc: AccountInfo) {
    this.confirmDialog.confirm(
      'Delete Account',
      'Do you really want to delete account ' + acc.username + ' (' + acc.id + ')?',
      'DELETE'
    ).then(() => {
      this.accountsService.deleteAccount(acc.id).subscribe();
    }, () => {
    });
  }
}
