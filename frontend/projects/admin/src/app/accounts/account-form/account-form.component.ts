import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormArray, FormBuilder, FormControl, ValidationErrors, Validators} from '@angular/forms';
import {AccountInfo, Role} from 'payment-lib';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.scss']
})
export class AccountFormComponent implements OnChanges {
  accountForm = this.fb.group({
    id: [null],
    username: ['', Validators.required],
    password: [],
    cardId: [],
    balance: [{value: 0, disabled: true}],
    overdraft: [0, Validators.compose([Validators.min(0), Validators.required])],
    locked: [false, Validators.required],
    roles: this.fb.array([])
  });
  @Input() public account: AccountInfo | null;
  @Input() public roles: Role[];
  @Input() public validationErrors: ValidationErrors;
  @Output() public save = new EventEmitter<AccountInfo>();

  constructor(private fb: FormBuilder) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.account) {
      this.accountForm.get('password').setValidators([Validators.minLength(8), Validators.required]);
    } else {
      this.accountForm.get('password').clearValidators();
      this.accountForm.patchValue(this.account);
    }
    const formRoles = this.accountForm.get('roles') as FormArray;
    if (Array.isArray(this.roles)) {
      this.roles.forEach(role => {
        if (this.account && this.account.roles.filter(ar => ar.id === role.id).length > 0) {
          formRoles.push(new FormControl(role));
        } else {
          formRoles.push(new FormControl(null));
        }
      });
    }

    if (changes.validationErrors && changes.validationErrors.currentValue !== changes.validationErrors.previousValue) {
      for (const key in this.validationErrors) {
        if (this.validationErrors.hasOwnProperty(key)) {
          const field = this.accountForm.get(key);
          field.setErrors({backendErrors: this.validationErrors[key]});
        }
      }
    }
  }

  saveAccount(): void {
    this.save.emit(this.accountForm.getRawValue());
  }

  changeRoles(role: Role, index: number, e) {
    const formRoles = this.accountForm.get('roles') as FormArray;
    if (e.target.checked) {
      formRoles.controls[index].setValue(role);
    } else {
      formRoles.controls[index].setValue(null);
    }
  }

  getAccountFormRolesArray(): FormArray {
    return this.accountForm.get('roles') as FormArray;
  }
}
