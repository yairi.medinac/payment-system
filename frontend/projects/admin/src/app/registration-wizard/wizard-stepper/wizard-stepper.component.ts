import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {interval, Observable, Subject} from 'rxjs';
import {debounce, distinctUntilChanged, filter, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import * as HttpStatus from 'http-status-codes';
import {FormControl, ValidationErrors, Validators} from '@angular/forms';
import {
  AccountFilter,
  AccountInfo,
  AccountsService,
  CardInfo,
  CreateChargeTransaction,
  Filter,
  PagedResult,
  PointOfSaleFilter,
  PointOfSaleInfo,
  PointsOfSaleService,
  RelationalOperator,
  RoleFilter,
  RolesService,
  SortDirection,
  TransactionsService
} from 'payment-lib';
import {NfcReaderService} from '../../services/nfc-reader.service';

@Component({
  selector: 'app-wizard-stepper',
  templateUrl: './wizard-stepper.component.html',
  changeDetection: ChangeDetectionStrategy.Default,
  styleUrls: ['./wizard-stepper.component.scss'],
  providers: [NfcReaderService]
})
export class WizardStepperComponent implements OnInit, OnDestroy {
  @ViewChild('stepper') stepper: ElementRef;
  searchUser = new Subject<KeyboardEvent>();
  userResults$: Observable<PagedResult<AccountInfo>>;
  posConnected: Observable<boolean>;
  account: AccountInfo;
  roles = this.rolesService.getAllRolesFiltered(new Filter(RoleFilter));
  selectedIndex = 0;
  accountFormValidationErrors: ValidationErrors;
  cardIdInputControl = new FormControl('', Validators.compose([Validators.required, Validators.minLength(1)]));
  searchUserInputControl = new FormControl();
  chargeAmountInputControl = new FormControl(0, Validators.compose([Validators.min(0), Validators.max(100)]));

  private currentCard: CardInfo | null;
  private destroy$ = new Subject();
  private userResults: PagedResult<AccountInfo>;

  constructor(private accountService: AccountsService,
              private nfcReaderService: NfcReaderService,
              private transactionService: TransactionsService,
              private posService: PointsOfSaleService,
              private rolesService: RolesService
  ) {
  }

  ngOnInit() {
    this.nfcReaderService.currentCard$.subscribe(cc => this.currentCard = cc);

    this.posConnected = this.posService.currentPos$.pipe(
      tap(pos => {
        if (pos !== null) {
          this.nfcReaderService.startHubConnection(pos.id);
        }
      }),
      switchMap(pos => this.nfcReaderService.connectionState$),
      takeUntil(this.destroy$)
    );

    this.searchUser.asObservable().pipe(
      takeUntil(this.destroy$),
      map(event => event.key),
      filter(key => key === 'Enter')
    ).subscribe(() => {
      if (this.userResults && this.userResults.rowCount === 1) {
        this.setAccount(this.userResults.results[0]);
      }
    });

    this.userResults$ = this.searchUser.asObservable().pipe(
      map(event => (event.target as HTMLInputElement).value),
      debounce(() => interval(300)),
      distinctUntilChanged(),
      filter(input => input !== ''),
      tap(console.log),
      switchMap(search => {
        const accFilter = new Filter(AccountFilter);
        accFilter.setPageSize(10);
        accFilter.setFilter('username', RelationalOperator.CONTAINS_CI, search);
        accFilter.setSort('username', SortDirection.ASC);
        return this.accountService.getFilteredAccounts(accFilter);
      }),
      tap(res => this.userResults = res)
    );

    this.posService.getAllPointsOfSaleFiltered(new Filter(PointOfSaleFilter)).pipe(
      // todo find a dynamic way to choose from pos'es
      map(posses => posses.length > 0 ? posses[0] : undefined),
      filter(pos => pos !== undefined),
      distinctUntilChanged((a, b) => a.id === b.id)
    ).subscribe(pos =>
      this.selectPos(pos)
    );
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  setAccount(account: AccountInfo) {
    this.account = account;
    this.selectedIndex = 1;
    this.stepper.nativeElement.focus();
  }

  useCurrentCard() {
    if (this.currentCard === null) {
      this.cardIdInputControl.setValue('NO_CARD_ON_READER');
      this.cardIdInputControl.markAsPristine();
    } else if (this.currentCard.account !== null && this.currentCard.account.id !== this.account.id) {
      this.cardIdInputControl.setValue('CARD_ALREADY_USED');
      this.cardIdInputControl.markAsPristine();
    } else {
      this.cardIdInputControl.setValue(this.currentCard.id);
      this.cardIdInputControl.markAsDirty();
    }
  }

  chargeAccount(amount: number) {
    if (!this.account || !this.account.id) {
      return;
    }
    if (amount === 0) {
      this.selectedIndex++;
      this.stepper.nativeElement.focus();
      return;
    }
    this.posService.currentPos$.pipe(
      switchMap(pos => {
        const charge: CreateChargeTransaction = {
          accountId: this.account.id,
          amount,
          comment: 'Charged account via wizard interface.',
          pointOfSaleId: pos.id
        };
        return this.transactionService.chargeAccount(charge);
      })
    ).subscribe(result => {
      this.account = result.account;
      this.selectedIndex++;
      this.stepper.nativeElement.focus();
    }, error => {
      this.chargeAmountInputControl.setValue(0);
    });
  }

  storeNewAccount(accountInfo: AccountInfo) {
    this.accountService.save(accountInfo).subscribe(acc => {
        this.account = acc;
        this.selectedIndex = 1;
        this.stepper.nativeElement.focus();
      },
      err => {
        if (err.status === HttpStatus.BAD_REQUEST) {
          console.log(err.error);
          this.accountFormValidationErrors = err.error.errors;
        }
      });
  }

  saveCard(cardId: string) {
    this.account.cardId = cardId;
    this.accountService.save(this.account).subscribe(result => {
      this.account = result;
      this.selectedIndex = 2;
      this.stepper.nativeElement.focus();
    }, error => {
      this.cardIdInputControl.setValue('ERROR_SAVING_CARD!!!');
    });
  }

  previousPage() {
    if (this.selectedIndex > 0) {
      this.selectedIndex--;
      this.stepper.nativeElement.focus();
    }
  }

  restartWizard() {
    this.account = undefined;
    this.userResults = undefined;
    this.searchUserInputControl.setValue('');
    this.chargeAmountInputControl.setValue(0);
    this.selectedIndex = 0;
    this.stepper.nativeElement.focus();
  }

  private selectPos(pos: PointOfSaleInfo) {
    this.posService.select(pos);
  }
}
