import {Pipe, PipeTransform} from '@angular/core';
import {Role} from 'payment-lib';

@Pipe({
  name: 'concatRoles'
})
export class ConcatRolesPipe implements PipeTransform {

  transform(value: Role[], ...args: any[]): any {
    return value.map(r => r.name).join(', ');
  }
}
