import {Component, EventEmitter, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {
  CreateProduct,
  Filter,
  Product,
  ProductsService,
  ProductType,
  ProductTypeFilter,
  ProductTypesService,
  UpdateProduct
} from 'payment-lib';
import {Observable} from 'rxjs';
import {BackendErrorHandler} from '../../helper/backend-error.handler';

@Component({
  selector: 'app-products-details',
  templateUrl: './products-details.component.html',
  styleUrls: ['./products-details.component.scss']
})
export class ProductsDetailsComponent implements OnInit {
  public product: Product;
  public productTypes$: Observable<ProductType[]>;

  productForm = this.fb.group({
    id: [null],
    name: ['', Validators.compose([
      Validators.required,
      Validators.minLength(3)
    ])],
    price: [null, Validators.required],
    description: [''],
    image: [],
    order: [0, Validators.compose([
      Validators.required
    ])],
    locked: [false, Validators.required],
    productType: [null],
  });
  public onImageUploadEvent: EventEmitter<any> = new EventEmitter<any>();
  public croppedImage: string | null | undefined;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private productsService: ProductsService,
    private productTypesService: ProductTypesService
  ) {
  }

  ngOnInit() {
    this.product = this.route.snapshot.data.product;
    this.productTypes$ = this.productTypesService.getAllProductTypesFiltered(new Filter(ProductTypeFilter));
    if (this.product !== null) {
      this.productForm.patchValue(this.product);
      this.productForm.patchValue({productType: this.product.productType.id});
    }
  }

  save(): void {
    const formValue = this.productForm.getRawValue() as Product;
    let response$: Observable<Product>;
    if (formValue.id === null) {
      const createMessage: CreateProduct = {
        description: formValue.description,
        image: formValue.image,
        locked: formValue.locked,
        name: formValue.name,
        order: formValue.order,
        price: formValue.price,
        productTypeId: this.productForm.get('productType').value
      };
      response$ = this.productsService.createProduct(createMessage);
    } else {
      const updateMessage: UpdateProduct = {
        description: formValue.description,
        image: formValue.image,
        locked: formValue.locked,
        name: formValue.name,
        order: formValue.order,
        price: formValue.price,
        productTypeId: this.productForm.get('productType').value
      };
      response$ = this.productsService.updateProduct(formValue.id, updateMessage);
    }
    response$.subscribe(
      (product) => {
        this.router.navigate(['/products', product.id]);
      },
      BackendErrorHandler.pushBackendErrorsToForm(this.productForm)
    );
  }

  imageCropped($event: ImageCroppedEvent) {
    this.croppedImage = $event.base64;
    this.productForm.get('image').setValue($event.base64);
  }
}
