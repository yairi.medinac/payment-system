import {Component} from '@angular/core';
import {NbAuthResult, NbLoginComponent} from '@nebular/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends NbLoginComponent {

  login(): void {
    this.errors = [];
    this.messages = [];
    this.submitted = true;

    this.service.authenticate(this.strategy, this.user).subscribe((result: NbAuthResult) => {
      this.submitted = false;

      if (result.isSuccess()) {

        const roles: string[] = [].concat.apply([], [result.getToken().getPayload().role]);
        if (roles.indexOf('Admin') !== -1) {
          this.messages = result.getMessages();
        } else {
          this.errors = ['You are not an admin user as thus wont need this interface!'];
          this.service.logout(this.strategy).subscribe();
          this.cd.detectChanges();
          return;
        }
      } else {
        this.errors = result.getErrors();
      }

      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
      this.cd.detectChanges();
    });
  }
}
