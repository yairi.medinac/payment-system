import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  items = [
    {
      title: 'Registration Wizard',
      icon: 'hat-wizard',
      link: 'registration-wizard',
    },
    {
      title: 'Accounts',
      icon: 'users',
      link: 'accounts',
    },
    {
      title: 'Orders',
      icon: 'credit-card',
      link: 'transactions',
    },
    {
      title: 'Products',
      icon: 'gem',
      link: 'products',
    },
    {
      title: 'Product Types',
      icon: 'archive',
      link: 'product-types',
    },
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
