import {Component, OnInit} from '@angular/core';
import {NbAuthService} from '@nebular/auth';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  public isAuthenticated$: Observable<boolean>;

  constructor(private nbAuthService: NbAuthService) {
  }

  ngOnInit() {
    this.isAuthenticated$ = this.nbAuthService.onAuthenticationChange();
  }

}
