import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ConfirmDialogService} from '../../layout/confirm-dialog/confirm-dialog.service';
import {merge, Observable} from 'rxjs';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import {FormControl} from '@angular/forms';
import {debounceTime, map, tap} from 'rxjs/operators';
import {
  PagedDataSource,
  ProductFilter,
  ProductType,
  ProductTypeFilter,
  ProductTypesService,
  RelationalOperator,
  SortDirection
} from 'payment-lib';

@Component({
  selector: 'app-product-types-list',
  templateUrl: './product-types-list.component.html',
  styleUrls: ['./product-types-list.component.scss']
})
export class ProductTypesListComponent implements OnInit, AfterViewInit {
  public columnsToDisplay = ['id', 'name', 'order', 'locked', 'actions'];
  public filterRow = ['id-filter', 'name-filter', 'order-filter', 'locked-filter', 'actions-filter'];
  public dataSource: PagedDataSource<ProductType, ProductTypeFilter>;

  allFilters$: Observable<[keyof ProductFilter, RelationalOperator, string | boolean]>;

  nameFilterValue = new FormControl('');
  nameFilterValue$: Observable<['name', RelationalOperator, string]>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private productTypesService: ProductTypesService, private confirmDialog: ConfirmDialogService) {
  }

  ngOnInit() {
    this.dataSource = new PagedDataSource(this.productTypesService.fetch, ProductTypeFilter);
    // this.dataSource.registerRoute();
    this.dataSource.filter.setSort('name', SortDirection.ASC);
    this.dataSource.loadData();
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.nameFilterValue$ = this.nameFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['name', RelationalOperator.CONTAINS_CI, v])
    );

    this.allFilters$ = merge(this.nameFilterValue$);

    merge(this.sort.sortChange, this.paginator.page, this.allFilters$)
      .pipe(
        tap((change: [keyof ProductTypeFilter, RelationalOperator, string] | Sort | PageEvent) => this.loadProductTypePage(change))
      )
      .subscribe();
  }

  loadProductTypePage<K extends keyof ProductTypeFilter>(change: [K, RelationalOperator, string | boolean] | Sort | PageEvent) {
    if (change instanceof Array) {
      const key = change[0] as K;
      const operator = change[1];
      const value = change[2];
      if (value === '') {
        this.dataSource.filter.clearFilter(key);
      } else {
        // @ts-ignore
        this.dataSource.filter.setFilter(key, operator, value);
      }
    }
    this.dataSource.filter.clearSort();
    // @ts-ignore
    this.dataSource.filter.setSort(this.sort.active, this.sort._direction);
    this.dataSource.filter.setPage(this.paginator.pageIndex + 1);
    this.dataSource.filter.setPageSize(this.paginator.pageSize);
    this.dataSource.loadData();
  }
}
