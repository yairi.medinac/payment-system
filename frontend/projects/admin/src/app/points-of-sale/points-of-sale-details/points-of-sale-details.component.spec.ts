import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointsOfSaleDetailsComponent } from './points-of-sale-details.component';

describe('PointsOfSaleDetailsComponent', () => {
  let component: PointsOfSaleDetailsComponent;
  let fixture: ComponentFixture<PointsOfSaleDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointsOfSaleDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsOfSaleDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
