import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CreatePointOfSale, PointOfSaleInfo, PointsOfSaleService, UpdatePointOfSale} from 'payment-lib';
import {ConfirmDialogService} from '../../layout/confirm-dialog/confirm-dialog.service';
import {Observable, Subject} from 'rxjs';
import {BackendErrorHandler} from '../../helper/backend-error.handler';

@Component({
  selector: 'app-points-of-sale-details',
  templateUrl: './points-of-sale-details.component.html',
  styleUrls: ['./points-of-sale-details.component.scss']
})
export class PointsOfSaleDetailsComponent implements OnInit, OnDestroy {

  oldPointOfSaleInfo: PointOfSaleInfo;

  private unsubscribe$ = new Subject<void>();

  pointOfSaleForm = this.fb.group({
    id: [null],
    name: ['', Validators.compose([
      Validators.required,
      Validators.minLength(3)
    ])],
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private pointsOfSaleService: PointsOfSaleService,
    private confirmDialog: ConfirmDialogService
  ) {
  }

  ngOnInit() {
    if (this.route.snapshot.data.pointOfSale === null) {
      this.oldPointOfSaleInfo = null;
      this.pointOfSaleForm.reset();
      return;
    } else {
      this.oldPointOfSaleInfo = this.route.snapshot.data.pointOfSale;
      this.pointOfSaleForm.patchValue(this.route.snapshot.data.pointOfSale);
    }
  }

  save(): void {
    const formValue = this.pointOfSaleForm.getRawValue() as PointOfSaleInfo;

    let response$: Observable<PointOfSaleInfo>;
    if (formValue.id === null) {
      const createMessage: CreatePointOfSale = {
        name: formValue.name
      };
      response$ = this.pointsOfSaleService.createPointOfSale(createMessage);
    } else {
      const updateMessage: UpdatePointOfSale = {
        name: formValue.name
      };
      response$ = this.pointsOfSaleService.updatePointOfSale(formValue.id, updateMessage);
    }
    response$.subscribe(
      (pos) => {
        this.router.navigate(['/points-of-sale', pos.id]);
      },
      BackendErrorHandler.pushBackendErrorsToForm(this.pointOfSaleForm)
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
