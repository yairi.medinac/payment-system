import {Component, OnDestroy, OnInit} from '@angular/core';
import {Filter, PointOfSaleFilter, PointOfSaleInfo, PointsOfSaleService} from 'payment-lib';
import {Subject} from 'rxjs';
import {switchMap, takeUntil} from 'rxjs/operators';
import {EventSubscriberService} from '../../services/event-subscriber.service';

@Component({
  selector: 'app-points-of-sale-list',
  templateUrl: './points-of-sale-list.component.html',
  styleUrls: ['./points-of-sale-list.component.scss']
})
export class PointsOfSaleListComponent implements OnInit, OnDestroy {
  public currentResult: PointOfSaleInfo[];
  private destroy = new Subject();

  constructor(
    private pointsOfSaleService: PointsOfSaleService,
    private eventSubscriberService: EventSubscriberService
  ) {
  }

  ngOnInit() {
    this.pointsOfSaleService.getAllPointsOfSaleFiltered(new Filter(PointOfSaleFilter)).subscribe(posses => {
      this.currentResult = posses;
    });

    this.eventSubscriberService.pointOfSaleEvents.pipe(
      takeUntil(this.destroy),
      switchMap(_ => {
        return this.pointsOfSaleService.getAllPointsOfSaleFiltered(new Filter(PointOfSaleFilter));
      })
    ).subscribe(posses => {
      this.currentResult = posses;
    });
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
}
