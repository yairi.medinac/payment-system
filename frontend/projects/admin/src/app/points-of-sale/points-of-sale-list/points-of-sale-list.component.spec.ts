import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointsOfSaleListComponent } from './points-of-sale-list.component';

describe('PointsOfSaleListComponent', () => {
  let component: PointsOfSaleListComponent;
  let fixture: ComponentFixture<PointsOfSaleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointsOfSaleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsOfSaleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
