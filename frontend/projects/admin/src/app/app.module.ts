import {BrowserModule} from '@angular/platform-browser';
import {DEFAULT_CURRENCY_CODE, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavigationComponent} from './layout/navigation/navigation.component';
import {AccountsDetailsComponent} from './accounts/accounts-details/accounts-details.component';
import {AccountsListComponent} from './accounts/accounts-list/accounts-list.component';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpRequest} from '@angular/common/http';
import {NgbModalModule, NgbPaginationModule, NgbPopoverModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {ConfirmDialogComponent} from './layout/confirm-dialog/confirm-dialog.component';
import {ProductsDetailsComponent} from './products/products-details/products-details.component';
import {ProductsListComponent} from './products/products-list/products-list.component';
import {ProductTypesDetailsComponent} from './product-types/product-types-details/product-types-details.component';
import {ProductTypesListComponent} from './product-types/product-types-list/product-types-list.component';
import {PointsOfSaleListComponent} from './points-of-sale/points-of-sale-list/points-of-sale-list.component';
import {PointsOfSaleDetailsComponent} from './points-of-sale/points-of-sale-details/points-of-sale-details.component';
import {TransactionListComponent} from './transactions/transaction-list/transaction-list.component';
import {TransactionDetailsComponent} from './transactions/transaction-details/transaction-details.component';
import {
  NbAccordionModule,
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbListModule,
  NbMenuModule,
  NbSelectModule,
  NbSidebarModule,
  NbStepperModule,
  NbThemeModule,
  NbToastrModule,
  NbToggleModule,
  NbTreeGridModule,
  NbUserModule
} from '@nebular/theme';
import {RouterModule} from '@angular/router';
import {SidebarComponent} from './layout/sidebar/sidebar.component';
import {ConcatRolesPipe} from './pipes/concat-roles.pipe';
import {ConcatProductsPipe} from './pipes/concat-products.pipe';
import {NB_AUTH_TOKEN_INTERCEPTOR_FILTER, NbAuthJWTInterceptor, NbAuthJWTToken, NbAuthModule, NbPasswordAuthStrategy} from '@nebular/auth';
import {AuthModule} from './auth/auth.module';
import {AuthGuard} from './services/auth-guard.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ImageCropperModule} from 'ngx-image-cropper';
import {WizardPageComponent} from './registration-wizard/wizard-page/wizard-page.component';
import {WizardStepperComponent} from './registration-wizard/wizard-stepper/wizard-stepper.component';
import {AccountFormComponent} from './accounts/account-form/account-form.component';
import {EventSubscriberService} from './services/event-subscriber.service';
import {environment} from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    AccountsDetailsComponent,
    AccountsListComponent,
    ProductsDetailsComponent,
    ProductsListComponent,
    ProductTypesDetailsComponent,
    ProductTypesListComponent,
    ConfirmDialogComponent,
    PointsOfSaleListComponent,
    PointsOfSaleDetailsComponent,
    TransactionListComponent,
    TransactionDetailsComponent,
    SidebarComponent,
    ConcatRolesPipe,
    ConcatProductsPipe,
    WizardPageComponent,
    WizardStepperComponent,
    AccountFormComponent,
    AccountFormComponent,
  ],
  imports: [
    RouterModule,
    BrowserModule,
    HttpClientModule,
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'user-pass',
          token: {
            class: NbAuthJWTToken,
            key: 'token',
          },
          baseEndpoint: '/api/v1',
          login: {
            endpoint: '/Login',
            redirect: {
              success: 'accounts',
            },
            defaultErrors: ['Login/Password combination is not correct, please try again.'],
          },
          logout: {
            endpoint: undefined,
            redirect: {
              success: 'auth/login'
            }
          },
          refreshToken: false
        }),
      ],
      forms: {
        login: {
          strategy: 'user-pass',
          redirectDelay: 0,
        },
        logout: {
          strategy: 'user-pass'
        }
      },
    }),
    AppRoutingModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule,
    NgbPopoverModule,
    NbLayoutModule,
    NbThemeModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbActionsModule,
    NbMenuModule.forRoot(),
    NbIconModule,
    NbTreeGridModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    NbButtonModule,
    AuthModule,
    NbToastrModule.forRoot(),
    MatTableModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    ImageCropperModule,
    NbToggleModule,
    MatCheckboxModule,
    NbStepperModule,
    NbCardModule,
    NbAccordionModule,
    NbListModule,
    NbUserModule
  ],
  providers: [
    AuthGuard,
    {
      provide: NB_AUTH_TOKEN_INTERCEPTOR_FILTER,
      // tslint:disable-next-line:object-literal-shorthand only-arrow-functions
      useValue: function(req: HttpRequest<any>) {
        return req.url === '/api/v1/Login';
      },
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: NbAuthJWTInterceptor,
      multi: true
    },
    EventSubscriberService,
    // todo: setting DEFAULT_CURRENCY_CODE is deprecated and will be removed in angular 10
    // we can remove this line in angular 10 as the currency will be taken from the current locale then
    // https://angular.io/api/core/DEFAULT_CURRENCY_CODE
    {provide: DEFAULT_CURRENCY_CODE, useValue: environment.defaultCurrency},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
