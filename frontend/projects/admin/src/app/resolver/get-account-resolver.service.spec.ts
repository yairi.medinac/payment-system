import { TestBed } from '@angular/core/testing';

import { GetAccountResolverService } from './get-account-resolver.service';

describe('GetAccountResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetAccountResolverService = TestBed.get(GetAccountResolverService);
    expect(service).toBeTruthy();
  });
});
