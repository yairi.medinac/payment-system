import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {PointOfSaleInfo, PointsOfSaleService} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class PointOfSaleResolverService implements Resolve<PointOfSaleInfo> {
  constructor(private pointOfSaleApi: PointsOfSaleService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<PointOfSaleInfo> | Observable<never> {
    if (route.params.id === 'new') {
      return null;
    }
    return this.pointOfSaleApi.getPointOfSaleById(route.params.id).pipe(
      catchError(e => {
        return of(null).pipe(
          tap(() => this.router.navigate(['404']))
        );
      })
    );
  }
}
