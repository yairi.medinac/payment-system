import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {TransactionResponse, TransactionsService} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class TransactionResolverService implements Resolve<TransactionResponse> {
  constructor(private transactionsService: TransactionsService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TransactionResponse> | Observable<never> {
    if (route.params.id === 'new') {
      return null;
    }
    return this.transactionsService.getTransactionById(route.params.id).pipe(
      catchError(e => {
        return of(null).pipe(
          tap(() => this.router.navigate(['404']))
        );
      })
    );
  }
}
