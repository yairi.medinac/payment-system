import {Injectable, OnDestroy, Optional} from '@angular/core';
import * as signalR from '@aspnet/signalr';
import {HubConnection, HubConnectionState} from '@aspnet/signalr';
import {HttpClient} from '@angular/common/http';
import {NbAuthJWTToken, NbAuthService} from '@nebular/auth';
import {CardInfo, uuid} from 'payment-lib';
import {BehaviorSubject} from 'rxjs';
import {NbToastrService} from '@nebular/theme';

@Injectable()
export class NfcReaderService implements OnDestroy {
  private connection: HubConnection = null;
  private currentCardSubject: BehaviorSubject<CardInfo | null> = new BehaviorSubject<CardInfo | null>(null);
  currentCard$ = this.currentCardSubject.asObservable();
  private connectionStateSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  connectionState$ = this.connectionStateSubject.asObservable();

  constructor(
    private http: HttpClient,
    private authService: NbAuthService,
    @Optional() private toastService: NbToastrService
  ) {
    console.log('initializing nfcReaderService');
    this.authService.onTokenChange().subscribe(async (token: NbAuthJWTToken) => {
        if (token.isValid()) {
          const roles: string[] = [].concat.apply([], [token.getPayload().role]);
          if (roles.indexOf('Admin') !== -1) {
            if (this.connection !== null && this.connection.state === HubConnectionState.Connected) {
              await this.connection.stop();
              this.connectionStateSubject.next(false);
            }
            this.connection = new signalR.HubConnectionBuilder()
              .withUrl('/payment-hub?access_token=' + token.getValue())
              .build();

            this.connection.onclose(err => {
              // TODO: handle loginError
            });

            this.connection.on('NewCard', (ev: CardInfo) => {
              this.currentCardSubject.next(ev);
            });

            this.connection.on('RemovedCard', () => {
              this.currentCardSubject.next(null);
            });
          }
        } else if (this.connection !== null && this.connection.state === HubConnectionState.Connected) {
          await this.connection.stop();
          this.connectionStateSubject.next(false);
          this.connection = null;
        }
      }
    );
  }

  public async startHubConnection(posId: uuid) {
    if (this.connection === null) {
      return;
    } else if (this.connection.state === HubConnectionState.Connected) {
      await this.connection.stop();
    }
    this.connection.start().then(value => {
      this.connection.invoke('JoinPointOfSale', {
        type: 'CashDesk',
        posId
      }).then(_ => {
        this.connectionStateSubject.next(true);
      }).catch(err => {
        if (this.toastService !== undefined) {
          this.toastService.danger(err, 'PoS Service Error', {duration: 15000});
        }
        console.error(err);
      });
    });
  }

  async ngOnDestroy() {
    console.log('NFC reader service is about to be destroyed.');
    if (this.connection !== null && this.connection.state === HubConnectionState.Connected) {
      this.connectionStateSubject.next(false);
      await this.connection.stop();
      this.connection = null;
    }
  }
}
