import { TestBed } from '@angular/core/testing';

import { EventSubscriberService } from './event-subscriber.service';

describe('EventSubscriberService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventSubscriberService = TestBed.get(EventSubscriberService);
    expect(service).toBeTruthy();
  });
});
