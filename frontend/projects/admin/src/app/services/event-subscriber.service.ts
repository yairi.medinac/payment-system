import {Injectable} from '@angular/core';
import * as signalR from '@aspnet/signalr';
import {HubConnection} from '@aspnet/signalr';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {NbAuthJWTToken, NbAuthService} from '@nebular/auth';
import {SubscriptionEventClasses, SubscriptionNotification, SubscriptionNotificationPointOfSaleInfo} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class EventSubscriberService {
  private connection: HubConnection = null;
  private pointOfSaleSubject: Subject<SubscriptionNotificationPointOfSaleInfo> = new Subject<SubscriptionNotificationPointOfSaleInfo>();

  constructor(private http: HttpClient, private authService: NbAuthService) {
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
        if (token.isValid()) {
          const roles: string[] = [].concat.apply([], [token.getPayload().role]);
          if (roles.indexOf('Admin') !== -1) {
            this.connection = new signalR.HubConnectionBuilder()
              .withUrl('/payment-hub?access_token=' + token.getValue())
              .build();
            this.startHubConnection();
          }
        } else if (this.connection !== null) {
          this.connection.stop().then(() => this.connection = null);
        }
      }
    );
  }

  get pointOfSaleEvents(): Observable<SubscriptionNotificationPointOfSaleInfo> {
    return this.pointOfSaleSubject.asObservable();
  }


  private startHubConnection() {
    this.connection.onclose(err => {
      // TODO: handle loginError
    });

    this.connection.on('NewEvent', (ev: SubscriptionNotification) => {
      console.log(ev);
      if (ev.eventClass === SubscriptionEventClasses.PointOfSaleInfo) {
        this.pointOfSaleSubject.next(ev);
      }
    });

    this.connection.start().then(value => {
      this.connection.invoke('SubscribeEvents').catch(err => console.error(err));
    }).catch(err => {
      // TODO: handle loginError
    });
  }
}
