# Payment Frontends
This project mainly consists of two UIs:
* Cashdesk
* AdminUi

## Development

Build the common payment-lib:
* `yarn run build:payment-lib`

Use the yarn jobs to serve developments servers for cashdesk/adminUi:
* `yarn run start:admin`
* `yarn run start:cashdesk`
